package com.viettel.vts.khdn.api.entity;

import java.math.BigInteger;
import java.util.Date;

public class EntitySalesOrderApprovementLineItem {
	private String salesOrderApprovementHeaderId;
	private String productId;
	private String unitId;
	private Integer orderQuantity;
	private Integer approvedQuantity;
	private BigInteger price;
	private Integer approvedTotal;
	private boolean isPromotion;
	private Date deliveryDate;
	private String status;
	private String note;
	private String salesOrderApprovementHeader;
	
	public String getSalesOrderApprovementHeaderId() {
		return salesOrderApprovementHeaderId;
	}
	public void setSalesOrderApprovementHeaderId(String salesOrderApprovementHeaderId) {
		this.salesOrderApprovementHeaderId = salesOrderApprovementHeaderId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Integer getOrderQuantity() {
		return orderQuantity;
	}
	public void setOrderQuantity(Integer orderQuantity) {
		this.orderQuantity = orderQuantity;
	}
	public Integer getApprovedQuantity() {
		return approvedQuantity;
	}
	public void setApprovedQuantity(Integer approvedQuantity) {
		this.approvedQuantity = approvedQuantity;
	}
	public BigInteger getPrice() {
		return price;
	}
	public void setPrice(BigInteger price) {
		this.price = price;
	}
	public Integer getApprovedTotal() {
		return approvedTotal;
	}
	public void setApprovedTotal(Integer approvedTotal) {
		this.approvedTotal = approvedTotal;
	}
	public boolean isPromotion() {
		return isPromotion;
	}
	public void setPromotion(boolean isPromotion) {
		this.isPromotion = isPromotion;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getSalesOrderApprovementHeader() {
		return salesOrderApprovementHeader;
	}
	public void setSalesOrderApprovementHeader(String salesOrderApprovementHeader) {
		this.salesOrderApprovementHeader = salesOrderApprovementHeader;
	}

	
}	
