package com.viettel.vts.khdn.api.entity;

import java.util.Date;

public class EntityInvoice {

	private String id;
	private String name;
	private Date createAt;
	private Date lastModifiedAt;
	private String salesOrderId;
	private Date salesOrderDate;
	private String deliveryOrderId;
	private Date deliveryOrderDate;
	private String issueOrderId;
	private Date issueOrderDate;
	private String invoiceNumber;
	private Date invoiceDate;
	private String channelId;
	private String customerId;
	private String shippingAddressId;
	private String contace;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public Date getLastModifiedAt() {
		return lastModifiedAt;
	}
	public void setLastModifiedAt(Date lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}
	public String getSalesOrderId() {
		return salesOrderId;
	}
	public void setSalesOrderId(String salesOrderId) {
		this.salesOrderId = salesOrderId;
	}
	public Date getSalesOrderDate() {
		return salesOrderDate;
	}
	public void setSalesOrderDate(Date salesOrderDate) {
		this.salesOrderDate = salesOrderDate;
	}
	public String getDeliveryOrderId() {
		return deliveryOrderId;
	}
	public void setDeliveryOrderId(String deliveryOrderId) {
		this.deliveryOrderId = deliveryOrderId;
	}
	public Date getDeliveryOrderDate() {
		return deliveryOrderDate;
	}
	public void setDeliveryOrderDate(Date deliveryOrderDate) {
		this.deliveryOrderDate = deliveryOrderDate;
	}
	public String getIssueOrderId() {
		return issueOrderId;
	}
	public void setIssueOrderId(String issueOrderId) {
		this.issueOrderId = issueOrderId;
	}
	public Date getIssueOrderDate() {
		return issueOrderDate;
	}
	public void setIssueOrderDate(Date issueOrderDate) {
		this.issueOrderDate = issueOrderDate;
	}
	public String getInvoiceNumber() {
		return invoiceNumber;
	}
	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}
	public Date getInvoiceDate() {
		return invoiceDate;
	}
	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getShippingAddressId() {
		return shippingAddressId;
	}
	public void setShippingAddressId(String shippingAddressId) {
		this.shippingAddressId = shippingAddressId;
	}
	public String getContace() {
		return contace;
	}
	public void setContace(String contace) {
		this.contace = contace;
	}
	
	
}
