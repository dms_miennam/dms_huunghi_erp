/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.aprocesscontroller;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigInteger;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.httpclient.util.DateUtil;
import org.json.JSONArray;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.xml.internal.stream.Entity;
import com.viettel.vts.khdn.constants.CommonRunnable;
import com.viettel.vts.khdn.database.DAO.CommonDAO;
import com.viettel.vts.khdn.database.DAO.DoDAO;
import com.viettel.vts.khdn.database.DAO.PoDAO;
import com.viettel.vts.khdn.database.DAO.RoDAO;
import com.viettel.vts.khdn.database.entity.EntityDo;
import com.viettel.vts.khdn.database.entity.EntityDoDetail;
import com.viettel.vts.khdn.database.entity.EntityPo;
import com.viettel.vts.khdn.database.entity.EntityPoDetail;
import com.viettel.vts.khdn.database.entity.EntityPoFilter;
import com.viettel.vts.khdn.database.entity.EntityPoReturn;
import com.viettel.vts.khdn.database.entity.EntityPoReturnDetail;
import com.viettel.vts.khdn.enumtype.ActiveType;
import com.viettel.vts.khdn.enumtype.SyncType;
import com.vittel.vts.khdn.api.entity.EntityDeliveryNoteLineItems;
import com.vittel.vts.khdn.api.entity.EntityDeliveryNotes;
import com.vittel.vts.khdn.api.entity.EntityDeliveryOrderLineItems;
import com.vittel.vts.khdn.api.entity.EntityDeliveryOrders;
import com.vittel.vts.khdn.api.entity.EntityInvoiceLineItems;
import com.vittel.vts.khdn.api.entity.EntityInvoices;
import com.vittel.vts.khdn.api.entity.EntityReturnOrderLineItems;
import com.vittel.vts.khdn.api.entity.EntityReturnOrders;
import com.vittel.vts.khdn.api.entity.EntitySalesOrderLineItems;
import com.vittel.vts.khdn.api.entity.EntitySalesOrders;

import transport.tcp.message.DeliverReport;


/**
 *
 * @author datnv5
 */
public class ThreadTask extends CommonRunnable {
	//Declare
	public static final String BASE_URL = "https://erpdms.huunghi.com.vn";
	private static String token;
	private static CommonDAO commonDAO = new CommonDAO();
    
    public ThreadTask(int noThread, int idxThread) {
        super(noThread, idxThread);
    }
    //gam
     public ThreadTask(int noThread, int idxThread,int a) {
                 super(noThread, idxThread);
    }
    
    @Override
    public void runThreadTask() {
        
        for (int i = 0; i < 999999999; i++) {
            try {
                System.out.println("run=" + i);
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ThreadTask.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        System.out.println("Thực hiện công việc");
    }
    
    public static void main(String[] args) throws IOException {
    	token = getToken();
    	System.out.println("token: "+ token  );
//    	salesOrders();
//    	deliveryOrders();
    	ReturnOrders();
    }
	
	public static String getToken() throws IOException {
		// Create A URL Object
//        URL url = new URL(BASE_URL + "/auth");
		URL url = new URL("https://erpdms.huunghi.com.vn/connect/token");
		
		// set Proxy
		System.setProperty("https.proxyHost", "10.61.11.42");
        System.setProperty("https.proxyPort", "3128");
 
        // Open a Connection
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
 
        // Set the Request Content-Type Header Parameter
        connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
        
 
        // Set Response Format Type
        connection.setRequestProperty("Accept", "application/json");
 
        // Set the Request Method
        connection.setRequestMethod("POST");
 
        // Create the Request Body and Send post request
//        grant_type = client_credentials
//        client_id = dms
//        client_secret= 388D45FA-B36B-4988-BA59-B187D329C207
        String urlParameters = "grant_type=client_credentials&client_id=dms&client_secret=388D45FA-B36B-4988-BA59-B187D329C207";
        sendRequest(connection, urlParameters);
        
        // set null proxy
//        System.setProperty("https.proxyHost", null);
 
        // Read the Response from Input Stream
        return getResponseToken(connection);
	}
	
	private static void sendRequest(HttpURLConnection connection, String data) throws IOException {
		// Ensure the Connection Will Be Used to Send Content
        connection.setDoOutput(true);
        // Create the Request Body and Send post request
        try (DataOutputStream wr = new DataOutputStream(connection.getOutputStream())) {
            wr.writeBytes(data);
            wr.flush();
        }
	}
	
	private static String getResponseToken(HttpURLConnection connection) throws IOException {
		int responseCode = connection.getResponseCode();
        System.out.println("Response Code : " + responseCode);
 
        StringBuilder response = new StringBuilder();
        try (InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));) {
            String line;
            while ((line = rd.readLine()) != null) {
            	if(line.contains("access_token")) {
            		// xử lý chuỗi access_token
            		String s[] = line.split("\""); 
            		response.append(s[3]);
            		break;
            	}
            }
        }
        return response.toString();
	}
	
	private static String getResponse(HttpURLConnection connection) throws IOException {
        int responseCode = connection.getResponseCode();
        System.out.println("Response Code : " + responseCode);
 
        StringBuilder response = new StringBuilder();
        try (InputStream is = connection.getInputStream();
                BufferedReader rd = new BufferedReader(new InputStreamReader(is));) {
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
            }
        }
        return response.toString();
    }
	
	// Duyệt đơn hàng, /api/SalesOrders/
	private static void salesOrders() throws IOException {
		URL url = new URL(BASE_URL + "/api/SalesOrders/?modifiedFrom=2021-03-30");
		
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
 
        connection.setRequestProperty("Authorization", "Bearer " + token);
        
        connection.setRequestProperty("Content-Type", "application/json");
 
        connection.setRequestMethod("GET");
        
        String response = getResponse(connection);
        
        System.out.println(response);
        
        final ObjectMapper objectMapper = new ObjectMapper();
         
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        
        List<EntitySalesOrders> lstSO = objectMapper.readValue(response, new TypeReference<List<EntitySalesOrders>>(){});
        if(lstSO != null && lstSO.size() > 0) {
        	for (EntitySalesOrders so : lstSO) {
        		if(so.getId() !=null) {
        			PoDAO poDAO = new PoDAO();
        			EntityPo poFilter = poDAO.getPoByCode(so.getDmsRefCode());
        			if(poFilter !=null && poFilter.getPo_number() == so.getDmsRefCode()) {
        				poFilter.setSo_number_erp(so.getCode());
        				poFilter.setPo_date(so.getDate().toString());
        				poFilter.setPo_number(so.getDmsRefCode());
        				poFilter.setChanel_code(so.getChannelId());
        				poFilter.setDelivery_address_code(so.getShippingAddressId());
        				poFilter.setContact_name(so.getContact());
        				switch (so.getStatus()) {
						case "0":
							poFilter.setStatus(ActiveType.WAIT_SEND.getValue());
							break;
						case "1":
							poFilter.setStatus(ActiveType.WAIT_APPROVE.getValue());
							break;
						case "2":
							poFilter.setStatus(ActiveType.APPROVED.getValue());
							break;
						case "3":
							poFilter.setStatus(ActiveType.PROCESSING.getValue());
							break;
						case "4":
							poFilter.setStatus(ActiveType.ENTERED.getValue());
							break;
						case "5":
							poFilter.setStatus(ActiveType.DONE.getValue());
							break;
						case "6":
							poFilter.setStatus(ActiveType.CANCEL.getValue());
							break;
						case "-1":
							poFilter.setStatus(ActiveType.DELETED.getValue());
							break;
						default:
							break;
						}
        				
        				poFilter.setIs_sync(SyncType.ERP_DUYET.getValue());
        				for(EntitySalesOrderLineItems item : so.getSalesOrderLineItems()) {
        					EntityPoDetail poDetailFilter = poDAO.getPoDetailByProductId(poFilter.getPo_id(),item.getProductId());
        					if(poDetailFilter != null) {
        						//line number ?
        						poDetailFilter.setProduct_id(Long.parseLong(item.getProductId()));
        						poDetailFilter.setUom(item.getUnitId());
        						poDetailFilter.setQuantity(item.getOrderQuantity());
        						poDetailFilter.setQuantity_approved(item.getApprovedQuantity());
        						//price ?
        						poDetailFilter.setAmount_approved(item.getAmount());
        						poDetailFilter.setPrice_type(item.getPriceTypeId());
        						// Con` gia chua biet sync
                				// poDetail.setprice
        						if(item.isPromotion()) {
        							poDetailFilter.setIs_promotion(1);
        						}else {
        							poDetailFilter.setIs_promotion(0);
        						}
        						// tax_id ?
        						poDetailFilter.setTax_num(item.getTaxId());
        						poDetailFilter.setTax_vat(item.getTaxRate());
        						poDetailFilter.setTax_amount(item.getTaxAmount());
        						poDetailFilter.setNote(item.getNote());
        						poDAO.updatePoDetail(poDetailFilter);
        					}
        				}
        				poDAO.updatePo(poFilter);
        			}
        		}
        	}
        }
    }
	
	// Xuất hàng và xuất hàng đột xuất /api/DeliveryOrders/
	private static void deliveryOrders() throws IOException {
URL url = new URL(BASE_URL + "/api/DeliveryOrders/?modifiedFrom=2021-03-30");
		
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
 
        connection.setRequestProperty("Authorization", "Bearer " + token);
        
        connection.setRequestProperty("Content-Type", "application/json");
 
        connection.setRequestMethod("GET");
        
        String response = getResponse(connection);
        
        System.out.println(response);
        
        final ObjectMapper objectMapper = new ObjectMapper();
         
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        
        List<EntityDeliveryOrders> lstDO = objectMapper.readValue(response, new TypeReference<List<EntityDeliveryOrders>>(){});
        if(lstDO != null && lstDO.size() > 0) {
        	for (EntityDeliveryOrders deliveryOrder : lstDO) {
        		if(deliveryOrder.getId() !=null) {
        			DoDAO doDAO = new DoDAO();
        			EntityDo doFilter = doDAO.getDoByCode(deliveryOrder.getCode());
        			if(doFilter == null) {
        				EntityDo doInsert = new EntityDo();
        				doInsert.setDo_number(deliveryOrder.getCode());
//        				salesorderId
        				if(deliveryOrder.getDeliveryOrderLineItems().get(0).getSalesOrderId() != null) {
        					String poId = commonDAO.getPoIdbySaleOrderId(deliveryOrder.getDeliveryOrderLineItems().get(0).getSalesOrderId());
        					doInsert.setPo_id(Long.parseLong(poId));
        				}
        				doInsert.setDo_date(deliveryOrder.getDate().toString());
        				doInsert.setChannel_code(deliveryOrder.getChannelId());
        				// shopId
        				String shopId = commonDAO.getShopIdByCustomerId(deliveryOrder.getCustomerId());
        				doInsert.setShop_id(Long.parseLong(shopId));
        				doInsert.setDelivery_address_code(deliveryOrder.getShippingAddressId());
        				// catId
        				String catId = commonDAO.getCatIdByProductGroupId(deliveryOrder.getProductGroupId());
        				doInsert.setCat_id(Long.parseLong(catId));
        				doInsert.setContact_name(deliveryOrder.getContact());
        				doInsert.setStatus(ActiveType.PROCESSING.getValue());
        				doInsert.setNote(deliveryOrder.getNote());
        				doInsert.setDo_date(deliveryOrder.getDate().toString());
        				doInsert.setUpdate_date(deliveryOrder.getLastModifiedAt().toString());
        				doInsert.setIs_sync(SyncType.ERP_DO.getValue());
        				Integer sumQuantity = 0;
        				BigInteger sumAmount = BigInteger.valueOf(0);
        				// insert Po
        				EntityPo poInsert = new EntityPo();
        				PoDAO poDAO = new PoDAO();
            			if(deliveryOrder.isUnscheduled()) {
            				poInsert.setPo_date(deliveryOrder.getDate().toString());
            				poInsert.setStatus(ActiveType.PROCESSING.getValue());
            				poInsert.setNote(deliveryOrder.getNote());
            				poInsert.setCreate_date(deliveryOrder.getCreateAt().toString());
            				poInsert.setUpdate_date(deliveryOrder.getLastModifiedAt().toString());
            				poInsert.setShop_id(Long.parseLong(shopId));
            				poInsert.setDelivery_address_code(deliveryOrder.getShippingAddressId());
            				poInsert.setChanel_code(deliveryOrder.getChannelId());
            				poInsert.setContact_name(deliveryOrder.getContact());
            				poInsert.setIs_sync(SyncType.ERP_DO.getValue());
            				poInsert.setCat_id(Long.parseLong(catId));
            			}
        				for(EntityDeliveryOrderLineItems item : deliveryOrder.getDeliveryOrderLineItems()) {
        					EntityDoDetail doDetail = new EntityDoDetail();
        					String productId = commonDAO.getProductIdByProductIdERP(item.getProductId());
        					doDetail.setProduct_id(Long.parseLong(productId));
        					doDetail.setUom(item.getUnitId());
        					
        					if(item.isPromotion()) {
        						doDetail.setIs_promotion(1);
        					}else {
        						doDetail.setIs_promotion(0);
        					}
        					doDetail.setPrice_type(item.getPriceTypeId());
        					if(item.getUnitId().equals("Kiện")) {
        						doDetail.setPackage_price(item.getPrice());
        						doDetail.setQuantity_pack_offer_export(item.getQuantity());
        					}else {
        						doDetail.setRetail_price(item.getPrice());
        						doDetail.setQuantity_retail_offer_export(item.getQuantity());
        					}
        					doDetail.setAmount_offer_export(item.getAmount());
        					doDetail.setNote(item.getNote());
        					doDetail.setTax_amount(item.getTaxAmount());
        					doDetail.setTax_num(item.getTaxId());
        					doDetail.setTax_vat(item.getTaxRate());
        					doDAO.insertDoDetail(doDetail);
        					if(deliveryOrder.isUnscheduled()) {
        						EntityPoDetail poDetailInsert = new EntityPoDetail();
        						poDetailInsert.setProduct_id(Long.parseLong(productId));
        						poDetailInsert.setUom(item.getUnitId());
        						if(item.isPromotion()) {
            						poDetailInsert.setIs_promotion(1);
            					}else {
            						poDetailInsert.setIs_promotion(0);
            					}
        						poDetailInsert.setPrice_type(item.getPriceTypeId());
        						if(item.getUnitId().equals("Kiện")) {
        							poDetailInsert.setPackage_price_erp(item.getPrice());
        							poDetailInsert.setPackage_quantity(item.getQuantity());
            					}else {
            						poDetailInsert.setRetail_price(item.getPrice());
            						poDetailInsert.setRetail_quantity(item.getQuantity());
            					}
        						poDetailInsert.setAmount(item.getAmount());
        						poDetailInsert.setNote(item.getNote());
        						poDetailInsert.setTax_amount(item.getTaxAmount());
        						poDetailInsert.setTax_num(item.getTaxId());
        						poDetailInsert.setTax_vat(item.getTaxRate());
        						poDAO.insertPoDetail(poDetailInsert);
        					}
        					sumQuantity += item.getQuantity();
        					sumAmount.add(item.getAmount());
        				}
        				if(deliveryOrder.isUnscheduled()) {
        					poInsert.setTax_num(deliveryOrder.getDeliveryOrderLineItems().get(0).getTaxId());
        					poInsert.setTax_vat(deliveryOrder.getDeliveryOrderLineItems().get(0).getTaxRate());
        					poInsert.setTax_amount(deliveryOrder.getDeliveryOrderLineItems().get(0).getTaxAmount());
        					poInsert.setAmount(sumAmount);
            				poInsert.setQuantity(sumQuantity);
            				poDAO.insertPo(poInsert);
        				}
        				doInsert.setTax_num(deliveryOrder.getDeliveryOrderLineItems().get(0).getTaxId());
    					doInsert.setTax_vat(deliveryOrder.getDeliveryOrderLineItems().get(0).getTaxRate());
    					doInsert.setTax_amount(deliveryOrder.getDeliveryOrderLineItems().get(0).getTaxAmount());
        				doInsert.setAmount_offer_export(sumAmount);
        				doInsert.setQuantity_offer_export(sumQuantity);
        				doDAO.insertDo(doInsert);
        			}
        		}
        	}
        }
	}

	// Duyệt đơn trả hàng /api/ReturnOrders
	private static void ReturnOrders() throws IOException {
		URL url = new URL(BASE_URL + "/api/ReturnOrders/?modifiedFrom=2021-03-30");
		
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
 
        connection.setRequestProperty("Authorization", "Bearer " + token);
        
        connection.setRequestProperty("Content-Type", "application/json");
 
        connection.setRequestMethod("GET");
        
        String response = getResponse(connection);
        
        System.out.println(response);
        
        final ObjectMapper objectMapper = new ObjectMapper();
         
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        
        List<EntityReturnOrders> lstRO = objectMapper.readValue(response, new TypeReference<List<EntityReturnOrders>>(){});
        if(lstRO != null && lstRO.size() > 0) {
        	for (EntityReturnOrders ro : lstRO) {
        		if(ro.getId() !=null) {
        			RoDAO roDAO = new RoDAO();
        			EntityPoReturn roFilter = roDAO.getRoByCode(ro.getDmsRefCode());
        			if(roFilter !=null && roFilter.getPo_return_number() == ro.getDmsRefCode()) {
        				
        				roFilter.setDo_date(ro.getDate().toString());
        				roFilter.setPo_return_notifi(ro.getReturnNotificationId());
        				roFilter.setChannel_code(ro.getChannelId());
        				String shopId = commonDAO.getShopIdByCustomerId(ro.getCustomerId());
        				roFilter.setShop_id(Long.parseLong(shopId));
        				roFilter.setDelivery_address_code(ro.getShippingAddressId());
        				roFilter.setContact_name(ro.getContact());
        				roFilter.setStatus(ActiveType.WAIT_APPROVE.getValue());
        				roFilter.setNote(ro.getNote());
        				roFilter.setCreate_date(ro.getCreatedAt().toString());
        				roFilter.setSync_date(ro.getLastModifiedAt().toString());
        				// isUnscheduled
        				roFilter.setIs_sync(SyncType.ERP_DUYET.getValue());
        				Integer sumQuantityOfferExport = 0;
        				BigInteger amountOfferExport = BigInteger.valueOf(0);
        				for(EntityReturnOrderLineItems item : ro.getReturnOrderLineItems()) {
        					String productId = commonDAO.getProductIdByProductIdERP(item.getProductId().toString());
        					EntityPoReturnDetail roDetailFilter = roDAO.getRoDetailByProductId(roFilter.getPo_return_id().toString() ,productId);
        					if(roDetailFilter != null) {
        						roDetailFilter.setUom(item.getUnitId());
        						roDetailFilter.setQuantity_approved(item.getQuantity());
        						if(item.isPromotion()) {
        							roDetailFilter.setIs_promotion(1);
        						}else {
        							roDetailFilter.setIs_promotion(0);
        						}
        						roDetailFilter.setPrice_type(item.getPriceTypeId());
        						roDetailFilter.setTax_num(item.getTaxId());
        						roDetailFilter.setTax_vat(item.getTaxRate());
        						roDetailFilter.setTax_amount(item.getTaxAmount());
        						roDAO.updateRoDetail(roDetailFilter);
        						sumQuantityOfferExport += item.getQuantity();
        						amountOfferExport.add(item.getAmount());
        					}
        				}
        				roFilter.setQuantity_return(sumQuantityOfferExport);
        				roFilter.setAmount_return(amountOfferExport);
        				roFilter.setDelivery_date(ro.getReturnOrderLineItems().get(0).getShipDate().toString());
        				roFilter.setShip_date(ro.getReturnOrderLineItems().get(0).getShipDate().toString());
        				roFilter.setTax_num(ro.getReturnOrderLineItems().get(0).getTaxId());
        				roFilter.setTax_vat(ro.getReturnOrderLineItems().get(0).getTaxRate());
        				roFilter.setTax_amount(ro.getReturnOrderLineItems().get(0).getTaxAmount());
        				roFilter.setTotal_return(amountOfferExport.subtract(roFilter.getTax_amount()));
        				
        				roDAO.updateRo(roFilter);
        			}
        		}
        	}
        }
    }
	
	// Cập nhật số lượng thực xuất API/DeliveryNote
	private static void DeliveryNotes() throws IOException {
		URL url = new URL(BASE_URL + "/api/DeliveryNotes?modifiedFrom=2021-03-30");
		
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
 
        connection.setRequestProperty("Authorization", "Bearer " + token);
        
        connection.setRequestProperty("Content-Type", "application/json");
 
        connection.setRequestMethod("GET");
        
        String response = getResponse(connection);
        
        System.out.println(response);
        
        final ObjectMapper objectMapper = new ObjectMapper();
         
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        
        List<EntityDeliveryNotes> lstDN = objectMapper.readValue(response, new TypeReference<List<EntityDeliveryNotes>>(){});
        
        if(lstDN != null && lstDN.size() > 0) {
        	for (EntityDeliveryNotes deliveryNote : lstDN) {
        		if(deliveryNote.getId() !=null) {
        			DoDAO doDAO = new DoDAO();
        			EntityDo doFilter = doDAO.getDoByCode(deliveryNote.getDeliveryOrderId());
        			if(doFilter != null && doFilter.getDo_id() != null) {
        				doFilter.setIssue_code(deliveryNote.getCode());
        				doFilter.setIssue_date(deliveryNote.getDate().toString());
        				doFilter.setChannel_code(deliveryNote.getChannelId());
        				String shopId = commonDAO.getShopIdByCustomerId(deliveryNote.getCustomerId());
        				doFilter.setShop_id(Long.parseLong(shopId));
        				doFilter.setDelivery_address_code(deliveryNote.getShippingAddressId());
        				String catId = commonDAO.getCatIdByProductGroupId(deliveryNote.getProductGroupId());
        				doFilter.setCat_id(Long.parseLong(catId));
        				doFilter.setContact_name(deliveryNote.getContact());
        				doFilter.setStatus(ActiveType.WAIT_SEND.getValue());
        				doFilter.setNote(deliveryNote.getNote());
        				doFilter.setUpdate_date(deliveryNote.getCreatedAt().toString());
        				doFilter.setSync_date(deliveryNote.getLastModifiedAt().toString());
        				doFilter.setIs_sync(SyncType.ERP_UPDATE_DO.getValue());
//	        				
        				Integer sumQuantityRealExport = 0;
        				BigInteger sumAmountRealExport = BigInteger.valueOf(0);
        				for(EntityDeliveryNoteLineItems item : deliveryNote.getDeliveryNoteLineItems()) {
        					String productId = commonDAO.getProductIdByProductIdERP(item.getProductId());
        					EntityDoDetail doDetailFilter = doDAO.getDoDetailByDoId(doFilter.getDo_id(), productId);
        					if(doDetailFilter != null && doDetailFilter.getDo_detail_id() != null) {
        						doDetailFilter.setProduct_id(Long.parseLong(productId));
        						doDetailFilter.setUom(item.getUnitId());
        						doDetailFilter.setQuantity_real_export(item.getQuantity());
        						doDAO.updateDoDetail(doDetailFilter);
        					sumQuantityRealExport += item.getQuantity();
//	        					sumAmountRealExport.add(item.getAmount());
        				}
//	        				doInsert.setAmount_offer_export(sumAmount);
        				doFilter.setQuantity_real_export(sumQuantityRealExport);
        				doDAO.updateDo(doFilter);
        			}
        		}
        	}
        }
    }
}
		
	//  Cập nhật số hóa đơn API/Invoices
	private static void Invoices() throws IOException {
		URL url = new URL(BASE_URL + "/api/Invoices/?modifiedFrom=2021-03-30");
		
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
 
        connection.setRequestProperty("Authorization", "Bearer " + token);
        
        connection.setRequestProperty("Content-Type", "application/json");
 
        connection.setRequestMethod("GET");
        
        String response = getResponse(connection);
        
        System.out.println(response);
        
        final ObjectMapper objectMapper = new ObjectMapper();
         
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        
        List<EntityInvoices> lstInvoices = objectMapper.readValue(response, new TypeReference<List<EntityInvoices>>(){});
        
        if(lstInvoices != null && lstInvoices.size() > 0) {
        	for (EntityInvoices invoice : lstInvoices) {
        		if(invoice.getId() !=null) {
        			DoDAO doDAO = new DoDAO();
        			EntityDo doFilter = doDAO.getDoByCode(invoice.getDeliveryOrderId());
        			if(doFilter != null && doFilter.getDo_id() != null) {
        				doFilter.setInvoice_number(invoice.getCode());
        				doFilter.setInvoice_date(invoice.getDate().toString());
        				doFilter.setChannel_code(invoice.getChannelId());
        				String shopId = commonDAO.getShopIdByCustomerId(invoice.getCustomerId());
        				doFilter.setShop_id(Long.parseLong(shopId));
//			        				doFilter.setDelivery_address_code(invoice.getShippingAddressId());
//			        				String catId = commonDAO.getCatIdByProductGroupId(invoice.getProductGroupId());
//			        				doFilter.setCat_id(Long.parseLong(catId));
//			        				doFilter.setContact_name(invoice.getContact());
        				doFilter.setStatus(ActiveType.WAIT_SEND.getValue());
//			        				doFilter.setNote(invoice.getNote());
        				doFilter.setUpdate_date(invoice.getCreatedAt().toString());
        				doFilter.setSync_date(invoice.getLastModifiedAt().toString());
        				doFilter.setIs_sync(SyncType.ERP_UPDATE_DO.getValue());
        				for(EntityInvoiceLineItems item : invoice.getInvoiceLineItems()) {
        					String productId = commonDAO.getProductIdByProductIdERP(item.getProductId());
        					EntityDoDetail doDetailFilter = doDAO.getDoDetailByDoId(doFilter.getDo_id(), productId);
        					if(doDetailFilter != null && doDetailFilter.getDo_detail_id() != null) {
        						doDetailFilter.setProduct_id(Long.parseLong(productId));
        						doDetailFilter.setUom(item.getUnitId());
        						//doDetailFilter.setQuantity_invoice(item.getQuantity());
        						doDetailFilter.setPrice_type(item.getPriceTypeId());
        						if(item.getUnitId().equals("Kiện")) {
        							doDetailFilter.setPackage_price(item.getPrice());
        						}else {
        							doDetailFilter.setRetail_price(item.getPrice());
        						}
//			        						doDetailFilter.setAmount_invoice(item.getAmount());
//			        						doDetailFilter.setNote(response);
        						doDAO.updateDoDetail(doDetailFilter);
        				}
        				doDAO.updateDo(doFilter);
        			}
        		}
        	}
        }
    }
}
}