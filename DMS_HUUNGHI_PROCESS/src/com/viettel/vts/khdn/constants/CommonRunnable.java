/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.constants;

import com.viettel.vts.khdn.aprocess.ManagerThreadRun;
import com.viettel.vts.khdn.constants.FunctionCommon;

/**
 *
 * @author KITTY
 */
public class CommonRunnable implements Runnable {

    private Thread t;
    private final String threadName;
    private Boolean run;
    private int idThread;
    private int numberThread;

    public CommonRunnable(int noThread, int idxThread) {
        numberThread = noThread;
        idThread = idxThread;
        run = false;
        threadName = FunctionCommon.KEYIDPROCESS + "ProcessSendSms" + idThread;
    }

    @Override
    public void run() {
        if (!run) {
            run = true;
            runThreadTask();
            System.out.println("=================complate=====" + threadName);
            run = false;
        }
    }

    public void start() {
        if (t == null) {
            t = new Thread(this, threadName);
        }
        t.start();
    }

    public void runThreadTask() {

    }

    public Boolean getRun() {
        return run;
    }

    public void setRun(Boolean run) {
        this.run = run;
    }
    public void stop() {
        if (t != null) {
            t.stop();
            t = null;
            run = false;
        }
    }
}
