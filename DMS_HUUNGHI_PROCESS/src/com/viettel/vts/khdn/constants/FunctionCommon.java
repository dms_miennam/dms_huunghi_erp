/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.constants;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.IOException;
import java.io.InputStream;
import java.security.SecureRandom;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 *
 * @author datnv5
 */
public class FunctionCommon {

    private static Logger log = Logger.getLogger(FunctionCommon.class);
    public static ExecutorService service = Executors.newFixedThreadPool(50);
    private static final ResourceBundle RESOURCE_BUNDLE = getResourceBundle();
    public static final String KEYIDPROCESS = generateDocumentId();
    public static Properties props;
    //========================thuc hien cac chuc nang chung cho hanh dong xu ly

    private static ResourceBundle getResourceBundle() {
        ResourceBundle appConfigRB = ResourceBundle.getBundle(
                StringConstants.CONFIGFILEPROPERTIES);
        return appConfigRB;
    }

    /**
     * generate AutoId
     *
     * @return
     */
    public static String generateDocumentId() {
        UUID uuid = UUID.randomUUID();
        return uuid.toString();
    }

    /**
     * lay du lieu theo key từ client truyen len
     *
     * @param item
     * @param strJsonData
     * @return
     */
    public static Object jsonGetItem(String item, String strJsonData) {
        Object result = null;
        try {
            JSONObject obj = new JSONObject(strJsonData);
            if (!obj.isNull(item)) {
                result = obj.get(item);
            }
        } catch (Exception e) {
            log.error("Loi! jsonGetItem: " + e.getMessage());
        }
        return result;
    }

    /**
     * convert json to object
     *
     * @param strJsonData
     * @param classOfT
     * @return
     */
    public static Object convertJsonToObject(String strJsonData, Class<?> classOfT) {
        Object result = null;
        try {
            GsonBuilder builder = new GsonBuilder();
            Gson gson = builder.create();
            result = gson.fromJson(strJsonData, classOfT);
        } catch (Exception e) {
            log.error("Loi! jsonGetItem: " + e.getMessage());
        }
        return result;
    }

    /**
     * Kiem tra xem cot select trong database va class co cot tuong ung hay
     * khong
     *
     * @param rs
     * @param columnName
     * @return
     * @throws SQLException
     */
    public static boolean hasColumn(ResultSet rs, String columnName) throws SQLException {
        ResultSetMetaData rsmd = rs.getMetaData();
        int columns = rsmd.getColumnCount();
        for (int x = 1; x <= columns; x++) {
            String cl = rsmd.getColumnName(x);
            if (columnName.toLowerCase().equals(cl.toLowerCase())) {
                return true;
            }
        }
        return false;
    }

    //=========Nhom ham thao tac database==========================
    /**
     * Thuc hien convert date to date sql de day vao dieu kien
     *
     * @param date
     * @return
     */
    public static Timestamp convertDateToSql(Date date) {
        Timestamp result = null;
        try {
            result = new Timestamp(date.getTime());
        } catch (Exception e) {
            log.error("Loi! FuctionCommon.convertDateToSql: " + e.getMessage());
        }
        return result;
    }

    /**
     * Loai bo khoang trang du thua
     *
     * @param str
     * @return
     */
    public static String trimspace(String str) {
        str = str.replaceAll("\\s+", " ");
        str = str.replaceAll("(^\\s+|\\s+$)", "");
        return str;
    }

    /**
     * loai bo ky tu dac biet tim kiem sql
     *
     * @param input
     * @return
     */
    public static String escapeSql(String input) {
        String result = input.trim().replace("/", "//").replace("_", "/_").replace("%", "/%");
        return result;
    }

    /**
     * lay ra khoang replace va ban ghi can thay the chu y: cau truc sql thay
     * the se phai thoa man dang where staffid in(:ListStaffId) and : cac ky tu
     * trong chuoi in phai sat canh nhau
     *
     * @param strSql
     * @param variale
     * @return
     */
    public static int[] getReplaceSqlInArr(String strSql, String variale) {
        int[] result = new int[4];
        int indext = strSql.indexOf(variale);
        int i = indext;
        int spase = 0;
        int end = indext + variale.length() + 1;
        //ket thuc ten cot can ghep dieu kien
        int charEnd = 0;
        int strInOrNotin = 0;
        int start = 0;
        while (true) {
            char a_char = strSql.charAt(i);

            if (a_char == ' ') {
                spase++;
            }
            if (spase == 1) {
                //gap khoang trong dau thi lay luon vi tri khoang trong
                charEnd = i;
                spase = 2;
            }
            if (spase == 3) {
                //gap khoang trong tiep theo thi danh dau vi tri dau can thay
                start = i;
                break;
            }
            i--;
            if (a_char == '(') {
                strInOrNotin = i;
            }
        }
        result[0] = start;
        result[1] = charEnd;
        result[2] = end;
        result[3] = strInOrNotin;
        return result;
    }

    /**
     * sap xep thu tu Hmap
     *
     * @param hmap
     * @return
     */
    public static List<String> sortHmap(HashMap<Integer, Object> hmap) {
        List<String> result = new ArrayList<>();
        if (hmap != null) {
            Map<Integer, Object> map = new TreeMap<>(hmap);
            System.out.println("After Sorting:");
            Set set2 = map.entrySet();
            Iterator iterator2 = set2.iterator();

            while (iterator2.hasNext()) {
                Map.Entry me2 = (Map.Entry) iterator2.next();
                result.add(me2.getValue().toString());
            }

        }
        return result;
    }

    /**
     * ham thuc hien select sql theo dang like string
     *
     * @param strColumn
     * @return
     */
    public static String sql_SelectLike(String strColumn) {
        StringBuilder strSqlLike = new StringBuilder();
        strSqlLike.append(String.format(" TRANSLATE(lower(%s),'", strColumn));
        strSqlLike.append(StringConstants.strSpec);
        strSqlLike.append("', '");
        strSqlLike.append(StringConstants.strRepl);
        strSqlLike.append(String.format("') like :%S ESCAPE '/' ", strColumn));
        return strSqlLike.toString();
    }

    /**
     * remove dau ca chuoi text
     *
     * @param s
     * @return
     */
    public static String removeUnsign(String s) {
        StringBuilder sb = new StringBuilder(s);
        for (int i = 0; i < sb.length(); i++) {
            sb.setCharAt(i, convertUnsign(sb.charAt(i)));
        }
        return sb.toString();
    }

    /**
     * convert unsign 1 ky tu
     *
     * @param ch
     * @return
     */
    private static char convertUnsign(char ch) {
        int index = StringConstants.strSpec.indexOf(ch);
        if (index >= 0) {
            ch = StringConstants.REPLACEMENTS_URL[index];
        }
        return ch;
    }
    
     /**
     * Go bo dau tieng viet
     *
     * @param s
     * @return
     */
    public static String removeAccent(String s) {
        if (s == null) {
            return "";
        }
        String temp = Normalizer.normalize(s, Normalizer.Form.NFD);
        Pattern pattern = Pattern.compile("\\p{InCombiningDiacriticalMarks}+");
        return pattern.matcher(temp).replaceAll("").replace("đ", "d").replace("Đ", "D");
    }

    //======================Nhom ham thao tac ma hoa giai ma du lieu=========
    /**
     * Convert tu hexString ra byte[]
     *
     * @param s
     * @return
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     * Convert tu bytes ra Hex
     *
     * @param bytes
     * @return
     */
    public static String bytesToHex(byte[] bytes) {
        char[] hexArray = "0123456789ABCDEF".toCharArray();
        char[] hexChars = new char[bytes.length * 2];
        for (int j = 0; j < bytes.length; j++) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

    public static String createTokenRandom() {
        SecureRandom random = new SecureRandom();
        byte bytes[] = new byte[20];
        random.nextBytes(bytes);
        String token = Arrays.toString(bytes);
        return token;
    }

    /**
     * convert data to Json
     *
     * @param obj
     * @return
     */
    public static String generateJSONBase(Object obj) {
        GsonBuilder gsonBuilder = new GsonBuilder();
        Gson gson = gsonBuilder.create();
        String data;
        try {
            data = gson.toJson(obj);
        } catch (Exception e) {
            data = null;
            log.error("Loi! generateJSONBase", e);
        }
        return data;
    }

    /**
     * Lay ra danh sach value theo key tu chuoi json
     *
     * @param json
     * @param keys
     * @return
     * @throws JSONException
     */
    public static List<String> getValuesFromJSON(JSONObject json, String[] keys) throws JSONException {
        List<String> listValue = new ArrayList<>();
        if (json != null && keys != null && keys.length > 0) {
            for (String key : keys) {
                if (json.isNull(key)) {
                    listValue.add("");
                } else {
                    listValue.add(json.getString(key).trim());
                }
            }
        }
        return listValue;
    }

    /**
     * Ho tro: Lay tu json tra ve gia tri kieu Object tuong ung can convert
     *
     * @param json
     * @param keys
     * @param type
     * @return
     * @throws JSONException
     */
    private static Object getValuesFromItemJSONToObj(JSONObject json, String keys, Object type) {
        Object result = null;
        Object valueParams = jsonGetItem(keys, json.toString());
        if (type.equals(Integer.class)) {
            //du lieu kieu int
            try {
                Integer valueInt = Integer.valueOf(valueParams.toString());
                result = valueInt;
            } catch (Exception e) {
                result = null;
                log.error("Loi! lay du lieu client truyen len", e);
            }

        }
        if (type.equals(Long.class)) {
            //du lieu kieu Long
            try {
                Long valueLong = Long.valueOf(valueParams.toString());
                result = valueLong;
            } catch (Exception e) {
                result = null;
                log.error("Loi! lay du lieu client truyen len", e);
            }
        }
        if (type.equals(String.class)) {
            //du lieu kieu String
            try {
                String valueString = (String) valueParams;
                result = valueString;
            } catch (Exception e) {
                result = null;
                log.error("Loi! lay du lieu client truyen len", e);
            }
        }
        if (type.equals(Date.class)) {
            try {
                //du lieu kieu int
                SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
                String dateInString = valueParams.toString();
                Date valueDate = formatter.parse(dateInString);
                result = valueDate;
            } catch (ParseException ex) {
                log.error("Loi! convert doi tuong dang ngay o params client truyen len" + ex.getMessage());
                result = null;
            }
        }
        return result;
    }

    /**
     * lay du lieu theo key từ client truyen len
     *
     * @param item
     * @param strJsonData
     * @return
     */
    public static JSONArray jsonGetArray(String item, String strJsonData) {
        JSONArray result = null;
        try {
            JSONObject obj = new JSONObject(strJsonData);
            result = obj.getJSONArray(item);
        } catch (Exception e) {
            log.error("Loi! jsonGetArray: " + e.getMessage());
        }
        return result;
    }

    /**
     * Sinh cac dau hoi cham (?) de noi vao cau SQL
     *
     * @param size So luong dau hoi
     * @return
     */
    public static String generateQuestionMark(int size) {
        if (size == 0) {
            return "";
        }
        StringBuilder questionMarks = new StringBuilder();
        for (int i = 0; i < size - 1; i++) {
            questionMarks.append(" ?, ");
        }
        questionMarks.append(" ? ");
        return questionMarks.toString();
    }

    /**
     * doc file properties trong cau hinh thu muc default
     *
     * @param key
     * @return
     */
    public static String getPropertiesValue(String key) {
        String value = FunctionCommon.RESOURCE_BUNDLE.containsKey(key)
                ? RESOURCE_BUNDLE.getString(key)
                : StringConstants.STR_EMTY;
        if (value.trim().length() <= 0) {
            log.error("Not value with key:" + key + ", in file properties");
        }
        return value;
    }

    /**
     * lay gia tri trong the xml
     *
     * @param xml
     * @param tagName
     * @return
     */
    public static String getTagValue(String xml, String tagName) {
        try {
            return xml.split("<" + tagName + ">")[1].split("</" + tagName + ">")[0];
        } catch (Exception e) {
            return "";
        }
    }
    
    /**
     * check thoi gian hien tai trong ngay nam giua 2 thoi gian
     * @param strTimeStart
     * @param strTimeEnd
     * @return 
     */
    public static Boolean checkThisTimeBetweenTwoTime(String strTimeStart, String strTimeEnd) {
        Calendar now = Calendar.getInstance();

        int hour = now.get(Calendar.HOUR_OF_DAY); // Get hour in 24 hour format
        int minute = now.get(Calendar.MINUTE);

        Date date = parseDate(hour + ":" + minute);
        Date dateCompareOne = parseDate(strTimeStart);//"08:00"
        Date dateCompareTwo = parseDate(strTimeEnd);//"20:00"
        return dateCompareOne.before(date) && dateCompareTwo.after(date);
    }

    private static  Date parseDate(String date) {

        final String inputFormat = "HH:mm";
        SimpleDateFormat inputParser = new SimpleDateFormat(inputFormat, Locale.US);
        try {
            return inputParser.parse(date);
        } catch (java.text.ParseException e) {
            return new Date(0);
        }
    }
    
    /**
    * Read a properties file from the classpath and return a Properties object
    * @param filename
    * @return
    * @throws IOException
    */
    static public Properties readProperties(String filename) throws IOException{
        Properties properties = new Properties();
        ClassLoader loader = Thread.currentThread().getContextClassLoader();
        InputStream stream = loader.getResourceAsStream(filename);
        
        properties.load(stream);
        return properties;
    }
}
