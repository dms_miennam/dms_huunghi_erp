/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.constants;

/**
 *
 * @author datnv5
 */
public class StringConstants {

    public static String STR_RESULT_RETURN_FULL = "{\"result\":{\"mess\":%s,\"data\": %s}}";
    public static String STR_RESULT_RETURN_MESS = "{\"result\":{\"mess\":%s,\"data\":{}}}";
    public static String STR_EMTY = "";
    //chuoi xu ly sql
    public static String STR_SQLPARAM = ":%s";
    public static String STR_SQLASKPARAM = "?";
    //key tách khoa aes thanh 2 phan aesKey va Vi key
    public static String STR_VIAESKEYSPACE = "VIAESKEYSPACE";
    //key lay session tu header
    public static String STR_SESSIONID = "session_id";
    public static String STR_COOKIE = "cookie";
    public static String STR_JSESSIONID = "JSESSIONID=";
    public static String STR_URL_SSO = "";
    public static String STR_FILE_CONFIGPROPERTI = "com/viettel/voffice/database/config/config.properties";
    //========Nhom khai bao tim kiem trong SQL ==================
    public static final String strSpec = "áàảãạăắằẳẵặâấầẩẫậđéèẻẽẹêếềểễệíìỉĩịóòỏõọôốồổỗộơớờởỡợúù"
            + "ủũụưứừửữựýỳỷỹỵÁÀẢÃẠĂẮẰẲẴẶÂẤẦẨẪẬĐÉÈẺẼẸÊẾỀỂỄỆÍÌỈĨỊÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÝỲỶỸỴ";
    public static final String strRepl = "aaaaaaaaaaaaaaaaadeeeeeeeeeeeiiiiiooooooooooooooooouu"
            + "uuuuuuuuuyyyyyAAAAAAAAAAAAAAAAADEEEEEEEEEEEIIIIIOOOOOOOOOOOOOOOOOUUUUUUUUUUUYYYYY";
    public static char[] REPLACEMENTS_URL = strRepl.toCharArray();
    
    public static String CONFIGFILEPROPERTIES = "config";
}
