/**
 * 
 */
package com.viettel.vts.khdn.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum DebtType.
 * 
 * @author huytran
 */
public enum SyncType {
    
	ERP_DUYET (1),

	ERP_DO (2),
    
	ERP_UPDATE_DO (3),
	
	ERP_INVOICE (4),
	
	ERP_DO_UNEXPECTED (5),
	
	ERP_DO_CANCEL (6),
	
	ERP_DELETE_DO (-1);
    
    /** The value. */
    private Integer value;
    
    /** The values. */
    private static volatile Map<Integer, SyncType> values = null;
    
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    
    /**
     * Instantiates a new gender type.
     * 
     * @param value
     *            the value
     */
    SyncType(Integer value) {
        this.value = value;
    }
    
    /**
     * Parses the value.
     * 
     * @param value
     *            the value
     * @return the gender type
     */
    public static SyncType parseValue(Integer value) {
        if (values == null) {
            values = new HashMap<Integer, SyncType>(
                    SyncType.values().length);
            for (SyncType e : SyncType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
}
