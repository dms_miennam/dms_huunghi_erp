/**
 * 
 */
package com.viettel.vts.khdn.enumtype;

import java.util.HashMap;
import java.util.Map;

/**
 * The Enum ActiveType.
 * 
 * @author hunglm16
 * @description Quy dinh tinh trang tren toan bo he thong SO
 */
public enum ActiveType {
    //-1
	DELETED(-1),
	//0
	WAIT_SEND(0),
	//1
	WAIT_APPROVE(1),

	APPROVED(2), 
	
	PROCESSING(3),
	
	ENTERED(4),
	
	DONE(5),
	
	CANCEL(6); 
	
    /** The value. */
    private Integer value;
    /** The values. */
    private static volatile Map<Integer, ActiveType> values = null;
    /**
     * Gets the value.
     * 
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
    /**
     * Instantiates a new gender type.
     * 
     * @param value the value
     */
    ActiveType(Integer value) {
        this.value = value;
    }
    /**
     * Parses the value.
     * 
     * @author hunglm16
     * @param value the value
     * @return the gender type
     */
    public static ActiveType parseValue(Integer value) {
        if(value==null){
        	value = -1;
        }
    	if (values == null) {
            values = new HashMap<Integer, ActiveType>(
                    ActiveType.values().length);
            for (ActiveType e : ActiveType.values())
                values.put(e.getValue(), e);
        }
        return values.get(value);
    }
    /**
     * Kiem tra co ton tai tinh trang quy dinh.
     * 
     * @author hunglm16
     * @param value
     * @return flag
     */
}
