/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.aprocess;

import com.viettel.vts.khdn.constants.FunctionCommon;
import java.util.Date;

/**
 *
 * @author datnv5
 */
public class CommonThread implements Runnable {

    private Thread t;
    private final String threadName;
    private Boolean run;

    CommonThread() {
        run = false;
        threadName = "CommonThread_" + FunctionCommon.KEYIDPROCESS;
    }

    @Override
    public void run() {
        if (!run) {
            run = true;
            runProcess();
            run = false;
        }
    }

    public void start() {
        if (t == null) {
            t = new Thread(this, threadName);
        }
        t.start();
    }

    public void stop() {
        ManagerThreadRun.stop();
        if (t != null) {
            t.stop();
            t = null;
            run = false;
        }
    }

    /**
     * cac ham nay thuc hien
     */
    public void runProcess() {
        System.out.println("===========runProcess============");
        try {
            RunProcess.startDate = new Date();
            ManagerThreadRun.excuteListThread();
            System.out.println("=========Finish Process=========Sleep===========");
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            System.out.println(e);
        }
    }

    public Boolean getRun() {
        return run;
    }

    public void setRun(Boolean run) {
        this.run = run;
    }
}
