/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.aprocess;

import com.viettel.vts.khdn.constants.FunctionCommon;
import com.viettel.vts.khdn.database.DAO.ProcessManagerDAO;
import com.viettel.vts.khdn.database.entity.EntityProcessManager;
import java.util.Date;
import org.apache.log4j.Logger;


/**
 *
 * @author KITTY
 */
public class RunProcess {
   private static Logger log = Logger.getLogger(RunProcess.class);
    public static Date startDate;
    public static void main(String[] args) {
        //thuc hien kiem tra va chay tien trinh
        ProcessManagerDAO processManagerDao = new ProcessManagerDAO();
        CommonThread t = new CommonThread();
        System.out.println("======chay tien trinh dong bo==========");
        while (true) {        
            String strKey = FunctionCommon.getPropertiesValue("process.code.app");
            EntityProcessManager entityProcessManager = processManagerDao.getSystemProcessmanager(strKey);
            System.out.println("=======Kiem tra lai trang thai  chay tien trinh===" + strKey);
            if(entityProcessManager != null){
                //co thong tin cau hinh
                //kiem tra thoi gian dang chay cua process hien tai
                //neu la thoi gian chạy nho hon 3 phut
                if(entityProcessManager.getIdnumber().equals(FunctionCommon.KEYIDPROCESS) 
                        && entityProcessManager.getDuringTime() <= 3){
                    //dung la tien trinh dang chay va thoi gian dang trong  thoi gian xu ly
                    //1.thuc hien update lai thoi gian
                    processManagerDao.updateProcessToData(strKey,FunctionCommon.KEYIDPROCESS);
                    //2.thuc hien chay tien trinh index du lieu
                    if(!t.getRun()){
                        System.out.println("===========tien trinh khong chay thi start==========");
                        t.stop();
                        try {
                            Thread.sleep(1000L);
                        } catch (InterruptedException ex) {
                            log.error(ex);
                        }
                        t.start();
                    }else{
                        Date endDate = new Date();
                        long diff = endDate.getTime() - startDate.getTime();
                        diff = diff / (1000 * 60 * 60);
                        if(diff>=2){
                            //sau 2 h khong ket thuc cong viec thi thoat
                            t.stop();
                            try {
                                Thread.sleep(1000L);
                            } catch (InterruptedException ex) {
                                log.error(ex);
                            }
                            System.out.println("===========Dong bo dang chay khoi tao lai dong bo==========");
                            t.start();
                        }else{
                            System.out.println("===========Dong bo dang chay==========" + diff);
                        }
                    }
                }else{
                    //neu thoi gian chay lon hon 3 phut thi khoi chay
                    if(!entityProcessManager.getIdnumber().equals(FunctionCommon.KEYIDPROCESS)
                            && entityProcessManager.getDuringTime() > 3){
                        processManagerDao.updateProcessToData(strKey,FunctionCommon.KEYIDPROCESS);
                    }
                    System.out.println("===========tien trinh dang o trang thai du phong==========");
                    //thuc hien stop lai cac tien trinh hien tai dang chay
                    t.stop();
                }
            }else{
                //khong co thong tin cau hinh
                //thuc hien chen du lieu vao
                boolean result = processManagerDao.insertProcessToData(strKey);
                System.out.println("neu chua co tien trinh nao chay thi khoi chay: " + result);
                t.stop();
            }
            try {
                Thread.sleep(20000L);
            } catch (InterruptedException ex) {
                log.error(ex);
            }
        }
    }
    
}
