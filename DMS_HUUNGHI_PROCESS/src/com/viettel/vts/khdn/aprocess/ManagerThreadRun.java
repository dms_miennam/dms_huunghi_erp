/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.aprocess;

import com.viettel.vts.khdn.constants.CommonRunnable;
import com.viettel.vts.khdn.aprocesscontroller.ThreadTask;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author datnv5
 */
public class ManagerThreadRun {
    static List<CommonRunnable> listThread = null;
    /**
     * gui tin nhan trong bang message
     */
    public static void excuteListThread() {
        //===================START THUC HIEN ADD CODE THREAD THUC HIEN O DAY================
        listThread = getListThreadExcute();
        //===================END THUC HIEN ADD CODE THREAD THUC HIEN O DAY================

        int numberThread = listThread.size();
        System.out.println("So Thread run send:" + numberThread);
        for (int i = 0; i < numberThread; i++) {
            listThread.get(i).start();
        }
        while (true) {
            //thuc hien chay lai tien trinh neu cac tien trinh da thuc hien xong
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
            Boolean isStop = true;
            for (CommonRunnable itemRunnable : listThread) {
                if (itemRunnable.getRun()) {
                    isStop = false;
                    break;
                }
            }
            if (isStop) {
                //tat ca cac tien trinh da thuc hien xong
                break;
            }
            try {
                Thread.sleep(2000L);
            } catch (InterruptedException ex) {
                System.out.println(ex);
            }
        }
    }
    
    static void stop() {
        if(listThread == null){
            return;
        }
        int numberThread = listThread.size();
        System.out.println("Dung cac tien trinh:" + numberThread);
        for (int i = 0; i < numberThread; i++) {
            listThread.get(i).stop();
        }
    }
    
    
    /**
     * Ham thuc hien tao cac thread
     * Co the thuc hien tao nhieu tien trinh trong 1 phien nhu demo ben duoi
     * DEV: thuc hien add cac code thread vao day
     * @return 
     */
    private static List<CommonRunnable> getListThreadExcute() {
        listThread = new ArrayList<>();
        ThreadTask threadExamSend1 = new ThreadTask(0, 2);
        listThread.add(threadExamSend1);
        return listThread;
    }

    
}
