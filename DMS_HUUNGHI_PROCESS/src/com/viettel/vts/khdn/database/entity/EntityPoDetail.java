package com.viettel.vts.khdn.database.entity;

import java.math.BigInteger;
import java.util.Date;

public class EntityPoDetail {
	private Long po_detail_id;
	private Long po_id;
	private Long shop_id;
	private Long cat_id;
	private Long product_id;
	private BigInteger amount;
	private Integer quantity;
	private Integer package_quantity;
	private Integer retail_quantity;
	private Integer package_price;
	private Integer retail_price;
	private Integer price_sale_in_id;
	private Integer convfact;
	private String create_date;
	private String create_user;
	private String update_date;
	private String update_user;
	private Integer po_line_number;
	private Integer quantity_approved;
	private Integer retail_quantity_approved;
	private Integer package_quantity_approved;
	private BigInteger amount_approved;
	private Integer package_price_erp;
	private Integer retail_price_erp;
	private String price_type;
	private String tax_num;
	private Integer tax_vat;
	private BigInteger tax_amount;
	private String product_info_code;
	private String uom;
	private Integer is_promotion;
	private String note;
	private String sync_date;
	private String sync_user;
	public Long getPo_detail_id() {
		return po_detail_id;
	}
	public void setPo_detail_id(Long po_detail_id) {
		this.po_detail_id = po_detail_id;
	}
	public Long getPo_id() {
		return po_id;
	}
	public void setPo_id(Long po_id) {
		this.po_id = po_id;
	}
	public Long getShop_id() {
		return shop_id;
	}
	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}
	public Long getCat_id() {
		return cat_id;
	}
	public void setCat_id(Long cat_id) {
		this.cat_id = cat_id;
	}
	public Long getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Long product_id) {
		this.product_id = product_id;
	}
	public BigInteger getAmount() {
		return amount;
	}
	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getPackage_quantity() {
		return package_quantity;
	}
	public void setPackage_quantity(Integer package_quantity) {
		this.package_quantity = package_quantity;
	}
	public Integer getRetail_quantity() {
		return retail_quantity;
	}
	public void setRetail_quantity(Integer retail_quantity) {
		this.retail_quantity = retail_quantity;
	}
	
	public Integer getPackage_price() {
		return package_price;
	}
	public void setPackage_price(Integer package_price) {
		this.package_price = package_price;
	}
	public Integer getRetail_price() {
		return retail_price;
	}
	public void setRetail_price(Integer retail_price) {
		this.retail_price = retail_price;
	}
	public Integer getPrice_sale_in_id() {
		return price_sale_in_id;
	}
	public void setPrice_sale_in_id(Integer price_sale_in_id) {
		this.price_sale_in_id = price_sale_in_id;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public String getCreate_date() {
		return create_date;
	}
	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}
	public String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	public String getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	public String getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	public Integer getPo_line_number() {
		return po_line_number;
	}
	public void setPo_line_number(Integer po_line_number) {
		this.po_line_number = po_line_number;
	}
	public Integer getQuantity_approved() {
		return quantity_approved;
	}
	public void setQuantity_approved(Integer quantity_approved) {
		this.quantity_approved = quantity_approved;
	}
	public Integer getRetail_quantity_approved() {
		return retail_quantity_approved;
	}
	public void setRetail_quantity_approved(Integer retail_quantity_approved) {
		this.retail_quantity_approved = retail_quantity_approved;
	}
	public Integer getPackage_quantity_approved() {
		return package_quantity_approved;
	}
	public void setPackage_quantity_approved(Integer package_quantity_approved) {
		this.package_quantity_approved = package_quantity_approved;
	}
	public BigInteger getAmount_approved() {
		return amount_approved;
	}
	public void setAmount_approved(BigInteger amount_approved) {
		this.amount_approved = amount_approved;
	}
	public Integer getPackage_price_erp() {
		return package_price_erp;
	}
	public void setPackage_price_erp(Integer package_price_erp) {
		this.package_price_erp = package_price_erp;
	}
	public Integer getRetail_price_erp() {
		return retail_price_erp;
	}
	public void setRetail_price_erp(Integer retail_price_erp) {
		this.retail_price_erp = retail_price_erp;
	}
	public String getPrice_type() {
		return price_type;
	}
	public void setPrice_type(String price_type) {
		this.price_type = price_type;
	}
	public String getTax_num() {
		return tax_num;
	}
	public void setTax_num(String tax_num) {
		this.tax_num = tax_num;
	}
	public Integer getTax_vat() {
		return tax_vat;
	}
	public void setTax_vat(Integer tax_vat) {
		this.tax_vat = tax_vat;
	}
	
	public BigInteger getTax_amount() {
		return tax_amount;
	}
	public void setTax_amount(BigInteger tax_amount) {
		this.tax_amount = tax_amount;
	}
	public String getProduct_info_code() {
		return product_info_code;
	}
	public void setProduct_info_code(String product_info_code) {
		this.product_info_code = product_info_code;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public Integer getIs_promotion() {
		return is_promotion;
	}
	public void setIs_promotion(Integer is_promotion) {
		this.is_promotion = is_promotion;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getSync_date() {
		return sync_date;
	}
	public void setSync_date(String sync_date) {
		this.sync_date = sync_date;
	}
	public String getSync_user() {
		return sync_user;
	}
	public void setSync_user(String sync_user) {
		this.sync_user = sync_user;
	}
	
}