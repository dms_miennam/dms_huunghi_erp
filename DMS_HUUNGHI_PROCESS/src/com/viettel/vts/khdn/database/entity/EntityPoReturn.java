package com.viettel.vts.khdn.database.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class EntityPoReturn {
	private Long po_return_id;
	private String po_return_number;
	private String po_return_date;
	private String po_return_notifi;
	private Long shop_id;
	private String channel_code;
	private String channel_name;
	private String delivery_address_code;
	private String delivery_address_name;
	private Integer quantity_return;
	private BigInteger amount_return;
	private String tax_num;
	private Integer tax_vat;
	private BigInteger tax_amount;
	private BigInteger total_return;
	private String contact_name;
	private Integer status;
	private String note;
	private String reason;
	private String create_date;
	private String create_user;
	private String update_date;
	private String update_user;
	private String sync_date;
	private String sync_user;
	private Integer is_sync;
	private String delivery_date;
	private String cat_id;
	private String ro_number_erp;
	private String do_date;
	private String ship_date;
	
	public String getShip_date() {
		return ship_date;
	}
	public void setShip_date(String ship_date) {
		this.ship_date = ship_date;
	}
	public String getDo_date() {
		return do_date;
	}
	public void setDo_date(String do_date) {
		this.do_date = do_date;
	}
	public Long getPo_return_id() {
		return po_return_id;
	}
	public void setPo_return_id(Long po_return_id) {
		this.po_return_id = po_return_id;
	}
	public String getPo_return_number() {
		return po_return_number;
	}
	public void setPo_return_number(String po_return_number) {
		this.po_return_number = po_return_number;
	}
	public String getPo_return_date() {
		return po_return_date;
	}
	public void setPo_return_date(String po_return_date) {
		this.po_return_date = po_return_date;
	}
	public String getPo_return_notifi() {
		return po_return_notifi;
	}
	public void setPo_return_notifi(String po_return_notifi) {
		this.po_return_notifi = po_return_notifi;
	}
	public Long getShop_id() {
		return shop_id;
	}
	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}
	public String getChannel_code() {
		return channel_code;
	}
	public void setChannel_code(String channel_code) {
		this.channel_code = channel_code;
	}
	public String getChannel_name() {
		return channel_name;
	}
	public void setChannel_name(String channel_name) {
		this.channel_name = channel_name;
	}
	public String getDelivery_address_code() {
		return delivery_address_code;
	}
	public void setDelivery_address_code(String delivery_address_code) {
		this.delivery_address_code = delivery_address_code;
	}
	public String getDelivery_address_name() {
		return delivery_address_name;
	}
	public void setDelivery_address_name(String delivery_address_name) {
		this.delivery_address_name = delivery_address_name;
	}
	public Integer getQuantity_return() {
		return quantity_return;
	}
	public void setQuantity_return(Integer quantity_return) {
		this.quantity_return = quantity_return;
	}
	public BigInteger getAmount_return() {
		return amount_return;
	}
	public void setAmount_return(BigInteger amount_return) {
		this.amount_return = amount_return;
	}
	public String getTax_num() {
		return tax_num;
	}
	public void setTax_num(String tax_num) {
		this.tax_num = tax_num;
	}
	public Integer getTax_vat() {
		return tax_vat;
	}
	public void setTax_vat(Integer tax_vat) {
		this.tax_vat = tax_vat;
	}
	public BigInteger getTax_amount() {
		return tax_amount;
	}
	public void setTax_amount(BigInteger tax_amount) {
		this.tax_amount = tax_amount;
	}
	public BigInteger getTotal_return() {
		return total_return;
	}
	public void setTotal_return(BigInteger total_return) {
		this.total_return = total_return;
	}
	public String getContact_name() {
		return contact_name;
	}
	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	public String getCreate_date() {
		return create_date;
	}
	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}
	public String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	public String getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	public String getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	public String getSync_date() {
		return sync_date;
	}
	public void setSync_date(String sync_date) {
		this.sync_date = sync_date;
	}
	public String getSync_user() {
		return sync_user;
	}
	public void setSync_user(String sync_user) {
		this.sync_user = sync_user;
	}
	public Integer getIs_sync() {
		return is_sync;
	}
	public void setIs_sync(Integer is_sync) {
		this.is_sync = is_sync;
	}
	public String getDelivery_date() {
		return delivery_date;
	}
	public void setDelivery_date(String delivery_date) {
		this.delivery_date = delivery_date;
	}
	public String getCat_id() {
		return cat_id;
	}
	public void setCat_id(String cat_id) {
		this.cat_id = cat_id;
	}
	public String getRo_number_erp() {
		return ro_number_erp;
	}
	public void setRo_number_erp(String ro_number_erp) {
		this.ro_number_erp = ro_number_erp;
	}
	
	
}