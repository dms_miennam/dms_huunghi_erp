package com.viettel.vts.khdn.database.entity;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Date;

public class EntityPo {
	private Long po_id;
	private Long shop_id;
	private String so_number_erp;
	private String po_number;
	private Integer po_type;
	private String po_date;
	private Integer status;
	private Long ref_asn_id;
	private String ref_asn_number;
	private BigInteger amount;
	private Integer quantity;
	private String note;
	private String create_date;
	private String create_user;
	private String update_date;
	private String update_user;
	private Integer approved_step;
	private String delivery_date;
	private String delivery_address_code;
	private String delivery_address_name;
	private String chanel_code;
	private String chanel_name;
	private String contact_name;
	private String tax_num;
	private Integer tax_vat;
	private BigInteger tax_amount;
	private Integer total;
	private Integer quantity_approved;
	private BigInteger amount_approved;
	private String approved_date;
	private String invoice_number;
	private String invoice_date;
	private String export_date;
	private String stock_date;
	private String sync_date;
	private String sync_user;
	private Integer is_sync;
	private Long cat_id;
	public Long getPo_id() {
		return po_id;
	}
	public void setPo_id(Long po_id) {
		this.po_id = po_id;
	}
	public Long getShop_id() {
		return shop_id;
	}
	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}
	public String getSo_number_erp() {
		return so_number_erp;
	}
	public void setSo_number_erp(String so_number_erp) {
		this.so_number_erp = so_number_erp;
	}
	public String getPo_number() {
		return po_number;
	}
	public void setPo_number(String po_number) {
		this.po_number = po_number;
	}
	public Integer getPo_type() {
		return po_type;
	}
	public void setPo_type(Integer po_type) {
		this.po_type = po_type;
	}
	public String getPo_date() {
		return po_date;
	}
	public void setPo_date(String po_date) {
		this.po_date = po_date;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public Long getRef_asn_id() {
		return ref_asn_id;
	}
	public void setRef_asn_id(Long ref_asn_id) {
		this.ref_asn_id = ref_asn_id;
	}
	public String getRef_asn_number() {
		return ref_asn_number;
	}
	public void setRef_asn_number(String ref_asn_number) {
		this.ref_asn_number = ref_asn_number;
	}
	
	public BigInteger getAmount() {
		return amount;
	}
	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getCreate_date() {
		return create_date;
	}
	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}
	public String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	public String getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	public String getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	public Integer getApproved_step() {
		return approved_step;
	}
	public void setApproved_step(Integer approved_step) {
		this.approved_step = approved_step;
	}
	public String getDelivery_date() {
		return delivery_date;
	}
	public void setDelivery_date(String delivery_date) {
		this.delivery_date = delivery_date;
	}
	public String getDelivery_address_code() {
		return delivery_address_code;
	}
	public void setDelivery_address_code(String delivery_address_code) {
		this.delivery_address_code = delivery_address_code;
	}
	public String getDelivery_address_name() {
		return delivery_address_name;
	}
	public void setDelivery_address_name(String delivery_address_name) {
		this.delivery_address_name = delivery_address_name;
	}
	public String getChanel_code() {
		return chanel_code;
	}
	public void setChanel_code(String chanel_code) {
		this.chanel_code = chanel_code;
	}
	public String getChanel_name() {
		return chanel_name;
	}
	public void setChanel_name(String chanel_name) {
		this.chanel_name = chanel_name;
	}
	public String getContact_name() {
		return contact_name;
	}
	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}
	public String getTax_num() {
		return tax_num;
	}
	public void setTax_num(String tax_num) {
		this.tax_num = tax_num;
	}
	public Integer getTax_vat() {
		return tax_vat;
	}
	public void setTax_vat(Integer tax_vat) {
		this.tax_vat = tax_vat;
	}
	public BigInteger getTax_amount() {
		return tax_amount;
	}
	public void setTax_amount(BigInteger tax_amount) {
		this.tax_amount = tax_amount;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public Integer getQuantity_approved() {
		return quantity_approved;
	}
	public void setQuantity_approved(Integer quantity_approved) {
		this.quantity_approved = quantity_approved;
	}
	public BigInteger getAmount_approved() {
		return amount_approved;
	}
	public void setAmount_approved(BigInteger amount_approved) {
		this.amount_approved = amount_approved;
	}
	public String getApproved_date() {
		return approved_date;
	}
	public void setApproved_date(String approved_date) {
		this.approved_date = approved_date;
	}
	public String getInvoice_number() {
		return invoice_number;
	}
	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}
	public String getInvoice_date() {
		return invoice_date;
	}
	public void setInvoice_date(String invoice_date) {
		this.invoice_date = invoice_date;
	}
	public String getExport_date() {
		return export_date;
	}
	public void setExport_date(String export_date) {
		this.export_date = export_date;
	}
	public String getStock_date() {
		return stock_date;
	}
	public void setStock_date(String stock_date) {
		this.stock_date = stock_date;
	}
	public String getSync_date() {
		return sync_date;
	}
	public void setSync_date(String sync_date) {
		this.sync_date = sync_date;
	}
	public String getSync_user() {
		return sync_user;
	}
	public void setSync_user(String sync_user) {
		this.sync_user = sync_user;
	}
	public Integer getIs_sync() {
		return is_sync;
	}
	public void setIs_sync(Integer is_sync) {
		this.is_sync = is_sync;
	}
	public Long getCat_id() {
		return cat_id;
	}
	public void setCat_id(Long cat_id) {
		this.cat_id = cat_id;
	}
	
}