/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.database.entity;

/**
 *
 * @author datnv5
 */
public class EntityConfigThread {
    Integer number_thread;
    String strListPhoneSend;

    public Integer getNumber_thread() {
        return number_thread;
    }

    public void setNumber_thread(Integer number_thread) {
        this.number_thread = number_thread;
    }

    public String getStrListPhoneSend() {
        return strListPhoneSend;
    }

    public void setStrListPhoneSend(String strListPhoneSend) {
        this.strListPhoneSend = strListPhoneSend;
    }
}
