package com.viettel.vts.khdn.database.entity;

import java.math.BigInteger;
import java.util.Date;

public class EntityDo {
	private Long do_id;
	private Long po_id;
	private Long shop_id;
	// catID
	private Long cat_id;
	private String do_number;
	private String do_code;
	private String do_date;
	private String issue_code;
	private String issue_date;
	private Integer type;
	private String channel_code;
	private String delivery_address_code;
	private String contact_name;
	private Integer quantity_offer_export;
	private BigInteger amount_offer_export;
	private Integer quantity_real_export;
	private BigInteger amount_real_export;
	private String tax_num;
	private Integer tax_vat;
	private BigInteger tax_amount;
	private Integer total_export;
	private Integer total_real;
	private Integer status;
	private String delivery_date;
	private String create_user;
	private String update_user;
	private String update_date;
	private String invoice_number;
	private String invoice_date;
	private String note;
	private String sync_date;
	private Integer is_sync;
	private String sync_user;
	public Long getDo_id() {
		return do_id;
	}
	public void setDo_id(Long do_id) {
		this.do_id = do_id;
	}
	public Long getPo_id() {
		return po_id;
	}
	public void setPo_id(Long po_id) {
		this.po_id = po_id;
	}
	public Long getShop_id() {
		return shop_id;
	}
	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}
	public Long getCat_id() {
		return cat_id;
	}
	public void setCat_id(Long cat_id) {
		this.cat_id = cat_id;
	}
	public String getDo_number() {
		return do_number;
	}
	public void setDo_number(String do_number) {
		this.do_number = do_number;
	}
	public String getDo_code() {
		return do_code;
	}
	public void setDo_code(String do_code) {
		this.do_code = do_code;
	}
	public String getDo_date() {
		return do_date;
	}
	public void setDo_date(String do_date) {
		this.do_date = do_date;
	}
	public String getIssue_code() {
		return issue_code;
	}
	public void setIssue_code(String issue_code) {
		this.issue_code = issue_code;
	}
	public String getIssue_date() {
		return issue_date;
	}
	public void setIssue_date(String issue_date) {
		this.issue_date = issue_date;
	}
	public Integer getType() {
		return type;
	}
	public void setType(Integer type) {
		this.type = type;
	}
	public String getChannel_code() {
		return channel_code;
	}
	public void setChannel_code(String channel_code) {
		this.channel_code = channel_code;
	}
	public String getDelivery_address_code() {
		return delivery_address_code;
	}
	public void setDelivery_address_code(String delivery_address_code) {
		this.delivery_address_code = delivery_address_code;
	}
	public String getContact_name() {
		return contact_name;
	}
	public void setContact_name(String contact_name) {
		this.contact_name = contact_name;
	}
	public Integer getQuantity_offer_export() {
		return quantity_offer_export;
	}
	public void setQuantity_offer_export(Integer quantity_offer_export) {
		this.quantity_offer_export = quantity_offer_export;
	}
	public BigInteger getAmount_offer_export() {
		return amount_offer_export;
	}
	public void setAmount_offer_export(BigInteger amount_offer_export) {
		this.amount_offer_export = amount_offer_export;
	}
	public Integer getQuantity_real_export() {
		return quantity_real_export;
	}
	public void setQuantity_real_export(Integer quantity_real_export) {
		this.quantity_real_export = quantity_real_export;
	}
	public BigInteger getAmount_real_export() {
		return amount_real_export;
	}
	public void setAmount_real_export(BigInteger amount_real_export) {
		this.amount_real_export = amount_real_export;
	}
	public String getTax_num() {
		return tax_num;
	}
	public void setTax_num(String tax_num) {
		this.tax_num = tax_num;
	}
	public Integer getTax_vat() {
		return tax_vat;
	}
	public void setTax_vat(Integer tax_vat) {
		this.tax_vat = tax_vat;
	}
	public BigInteger getTax_amount() {
		return tax_amount;
	}
	public void setTax_amount(BigInteger tax_amount) {
		this.tax_amount = tax_amount;
	}
	public Integer getTotal_export() {
		return total_export;
	}
	public void setTotal_export(Integer total_export) {
		this.total_export = total_export;
	}
	public Integer getTotal_real() {
		return total_real;
	}
	public void setTotal_real(Integer total_real) {
		this.total_real = total_real;
	}
	public Integer getStatus() {
		return status;
	}
	public void setStatus(Integer status) {
		this.status = status;
	}
	public String getDelivery_date() {
		return delivery_date;
	}
	public void setDelivery_date(String delivery_date) {
		this.delivery_date = delivery_date;
	}
	public String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	public String getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	public String getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	public String getInvoice_number() {
		return invoice_number;
	}
	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}
	public String getInvoice_date() {
		return invoice_date;
	}
	public void setInvoice_date(String invoice_date) {
		this.invoice_date = invoice_date;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getSync_date() {
		return sync_date;
	}
	public void setSync_date(String sync_date) {
		this.sync_date = sync_date;
	}
	public Integer getIs_sync() {
		return is_sync;
	}
	public void setIs_sync(Integer is_sync) {
		this.is_sync = is_sync;
	}
	public String getSync_user() {
		return sync_user;
	}
	public void setSync_user(String sync_user) {
		this.sync_user = sync_user;
	}
	
}