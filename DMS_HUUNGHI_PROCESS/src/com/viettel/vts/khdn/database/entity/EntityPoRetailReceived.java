package com.viettel.vts.khdn.database.entity;

import java.math.BigInteger;
import java.util.Date;

public class EntityPoRetailReceived {
	private Integer po_detail_received;
	private Long po_id;
	private Long do_id;
	private Long do_detail_id;
	private Long shop_id;
	private Long product_id;
	private BigInteger retail_price;
	private BigInteger pack_price;
	private String price_type;
	private Integer quantity_package;
	private Integer quantity_received_retail;
	private Integer quantity_received_pack;
	private BigInteger amount_received;
	private String tax_num;
	private Integer tax_vat;
	private Integer tax_amount;
	private Integer convfact;
	private Integer is_promotion;
	private Date received_date;
	private String invoice_number;
	private Date create_date;
	private String create_user;
	private Date update_date;
	private String update_user;
	private String note;
	private Date sync_date;
	private String sync_user;
	public Integer getPo_detail_received() {
		return po_detail_received;
	}
	public void setPo_detail_received(Integer po_detail_received) {
		this.po_detail_received = po_detail_received;
	}
	public Long getPo_id() {
		return po_id;
	}
	public void setPo_id(Long po_id) {
		this.po_id = po_id;
	}
	public Long getDo_id() {
		return do_id;
	}
	public void setDo_id(Long do_id) {
		this.do_id = do_id;
	}
	public Long getDo_detail_id() {
		return do_detail_id;
	}
	public void setDo_detail_id(Long do_detail_id) {
		this.do_detail_id = do_detail_id;
	}
	public Long getShop_id() {
		return shop_id;
	}
	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}
	public Long getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Long product_id) {
		this.product_id = product_id;
	}
	public BigInteger getRetail_price() {
		return retail_price;
	}
	public void setRetail_price(BigInteger retail_price) {
		this.retail_price = retail_price;
	}
	public BigInteger getPack_price() {
		return pack_price;
	}
	public void setPack_price(BigInteger pack_price) {
		this.pack_price = pack_price;
	}
	public String getPrice_type() {
		return price_type;
	}
	public void setPrice_type(String price_type) {
		this.price_type = price_type;
	}
	public Integer getQuantity_package() {
		return quantity_package;
	}
	public void setQuantity_package(Integer quantity_package) {
		this.quantity_package = quantity_package;
	}
	public Integer getQuantity_received_retail() {
		return quantity_received_retail;
	}
	public void setQuantity_received_retail(Integer quantity_received_retail) {
		this.quantity_received_retail = quantity_received_retail;
	}
	public Integer getQuantity_received_pack() {
		return quantity_received_pack;
	}
	public void setQuantity_received_pack(Integer quantity_received_pack) {
		this.quantity_received_pack = quantity_received_pack;
	}
	public BigInteger getAmount_received() {
		return amount_received;
	}
	public void setAmount_received(BigInteger amount_received) {
		this.amount_received = amount_received;
	}
	public String getTax_num() {
		return tax_num;
	}
	public void setTax_num(String tax_num) {
		this.tax_num = tax_num;
	}
	public Integer getTax_vat() {
		return tax_vat;
	}
	public void setTax_vat(Integer tax_vat) {
		this.tax_vat = tax_vat;
	}
	public Integer getTax_amount() {
		return tax_amount;
	}
	public void setTax_amount(Integer tax_amount) {
		this.tax_amount = tax_amount;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public Integer getIs_promotion() {
		return is_promotion;
	}
	public void setIs_promotion(Integer is_promotion) {
		this.is_promotion = is_promotion;
	}
	public Date getReceived_date() {
		return received_date;
	}
	public void setReceived_date(Date received_date) {
		this.received_date = received_date;
	}
	public String getInvoice_number() {
		return invoice_number;
	}
	public void setInvoice_number(String invoice_number) {
		this.invoice_number = invoice_number;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
	public String getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getSync_date() {
		return sync_date;
	}
	public void setSync_date(Date sync_date) {
		this.sync_date = sync_date;
	}
	public String getSync_user() {
		return sync_user;
	}
	public void setSync_user(String sync_user) {
		this.sync_user = sync_user;
	}
	
}

