package com.viettel.vts.khdn.database.entity;

import java.math.BigInteger;
import java.util.Date;

public class EntityDoDetail {
	private Long do_detail_id;
	private Long do_id;
	private Long po_id;
	private Long shop_id;
	private Long product_id;
	private Integer retail_price;
	private Integer package_price;
	private String price_type;
	private Integer quantity_retail_offer_export;
	private Integer quantity_pack_offer_export;
	private BigInteger amount_offer_export;
	private Integer quantity_real_export;
	private Integer quantity_pack_export;
	private Integer amount_real_export;
	private String uom;
	private Integer is_promotion;
	private String note;
	private Date create_date;
	private String create_user;
	private Date update_date;
	private String update_user;
	private Date sync_date;
	private String sync_user;
	private String tax_num;
	private Integer tax_vat;
	private BigInteger tax_amount;
	public Long getDo_detail_id() {
		return do_detail_id;
	}
	public void setDo_detail_id(Long do_detail_id) {
		this.do_detail_id = do_detail_id;
	}
	public Long getDo_id() {
		return do_id;
	}
	public void setDo_id(Long do_id) {
		this.do_id = do_id;
	}
	public Long getPo_id() {
		return po_id;
	}
	public void setPo_id(Long po_id) {
		this.po_id = po_id;
	}
	public Long getShop_id() {
		return shop_id;
	}
	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}
	public Long getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Long product_id) {
		this.product_id = product_id;
	}
	public Integer getRetail_price() {
		return retail_price;
	}
	public void setRetail_price(Integer retail_price) {
		this.retail_price = retail_price;
	}
	public Integer getPackage_price() {
		return package_price;
	}
	public void setPackage_price(Integer package_price) {
		this.package_price = package_price;
	}
	public String getPrice_type() {
		return price_type;
	}
	public void setPrice_type(String price_type) {
		this.price_type = price_type;
	}
	public Integer getQuantity_retail_offer_export() {
		return quantity_retail_offer_export;
	}
	public void setQuantity_retail_offer_export(Integer quantity_retail_offer_export) {
		this.quantity_retail_offer_export = quantity_retail_offer_export;
	}
	public Integer getQuantity_pack_offer_export() {
		return quantity_pack_offer_export;
	}
	public void setQuantity_pack_offer_export(Integer quantity_pack_offer_export) {
		this.quantity_pack_offer_export = quantity_pack_offer_export;
	}
	public BigInteger getAmount_offer_export() {
		return amount_offer_export;
	}
	public void setAmount_offer_export(BigInteger amount_offer_export) {
		this.amount_offer_export = amount_offer_export;
	}
	public Integer getQuantity_real_export() {
		return quantity_real_export;
	}
	public void setQuantity_real_export(Integer quantity_real_export) {
		this.quantity_real_export = quantity_real_export;
	}
	public Integer getQuantity_pack_export() {
		return quantity_pack_export;
	}
	public void setQuantity_pack_export(Integer quantity_pack_export) {
		this.quantity_pack_export = quantity_pack_export;
	}
	public Integer getAmount_real_export() {
		return amount_real_export;
	}
	public void setAmount_real_export(Integer amount_real_export) {
		this.amount_real_export = amount_real_export;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public Integer getIs_promotion() {
		return is_promotion;
	}
	public void setIs_promotion(Integer is_promotion) {
		this.is_promotion = is_promotion;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	public String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	public Date getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(Date update_date) {
		this.update_date = update_date;
	}
	public String getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	public Date getSync_date() {
		return sync_date;
	}
	public void setSync_date(Date sync_date) {
		this.sync_date = sync_date;
	}
	public String getSync_user() {
		return sync_user;
	}
	public void setSync_user(String sync_user) {
		this.sync_user = sync_user;
	}
	public String getTax_num() {
		return tax_num;
	}
	public void setTax_num(String tax_num) {
		this.tax_num = tax_num;
	}
	public Integer getTax_vat() {
		return tax_vat;
	}
	public void setTax_vat(Integer tax_vat) {
		this.tax_vat = tax_vat;
	}
	public BigInteger getTax_amount() {
		return tax_amount;
	}
	public void setTax_amount(BigInteger tax_amount) {
		this.tax_amount = tax_amount;
	}
	
}