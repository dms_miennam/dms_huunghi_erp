package com.viettel.vts.khdn.database.entity;

import java.math.BigInteger;
import java.util.Date;

public class EntityPoReturnDetail {
	private Long po_return_detail_id;
	private Long po_return_id;
	private Long shop_id;
	private Long product_id;
	private Long price_id;
	private Long cat_id;
	private Integer price_retail;
	private Integer price_pack;
	private Integer price_retail_approved;
	private Integer price_pack_approved;
	private Integer quantity_return;
	private Integer quantity_retail_return;
	private Integer quantity_pack_return;
	private BigInteger amount_return;
	private Integer quantity_approved;
	private Integer quantity_retail_approved;
	private Integer quantity_pack_approved;
	private BigInteger amount_approved;
	private Integer convfact;
	private String uom;
	private String price_type;
	private Integer is_promotion;
	private String tax_num;
	private Integer tax_vat;
	private BigInteger tax_amount;
	private String note;
	private String create_date;
	private String create_user;
	private String update_date;
	private String update_user;
	private String sync_date;
	private String sync_user;
	private String product_info_code;
	public Long getPo_return_detail_id() {
		return po_return_detail_id;
	}
	public void setPo_return_detail_id(Long po_return_detail_id) {
		this.po_return_detail_id = po_return_detail_id;
	}
	public Long getPo_return_id() {
		return po_return_id;
	}
	public void setPo_return_id(Long po_return_id) {
		this.po_return_id = po_return_id;
	}
	public Long getShop_id() {
		return shop_id;
	}
	public void setShop_id(Long shop_id) {
		this.shop_id = shop_id;
	}
	public Long getProduct_id() {
		return product_id;
	}
	public void setProduct_id(Long product_id) {
		this.product_id = product_id;
	}
	public Long getPrice_id() {
		return price_id;
	}
	public void setPrice_id(Long price_id) {
		this.price_id = price_id;
	}
	public Long getCat_id() {
		return cat_id;
	}
	public void setCat_id(Long cat_id) {
		this.cat_id = cat_id;
	}
	public Integer getPrice_retail() {
		return price_retail;
	}
	public void setPrice_retail(Integer price_retail) {
		this.price_retail = price_retail;
	}
	public Integer getPrice_pack() {
		return price_pack;
	}
	public void setPrice_pack(Integer price_pack) {
		this.price_pack = price_pack;
	}
	public Integer getPrice_retail_approved() {
		return price_retail_approved;
	}
	public void setPrice_retail_approved(Integer price_retail_approved) {
		this.price_retail_approved = price_retail_approved;
	}
	public Integer getPrice_pack_approved() {
		return price_pack_approved;
	}
	public void setPrice_pack_approved(Integer price_pack_approved) {
		this.price_pack_approved = price_pack_approved;
	}
	public Integer getQuantity_return() {
		return quantity_return;
	}
	public void setQuantity_return(Integer quantity_return) {
		this.quantity_return = quantity_return;
	}
	public Integer getQuantity_retail_return() {
		return quantity_retail_return;
	}
	public void setQuantity_retail_return(Integer quantity_retail_return) {
		this.quantity_retail_return = quantity_retail_return;
	}
	public Integer getQuantity_pack_return() {
		return quantity_pack_return;
	}
	public void setQuantity_pack_return(Integer quantity_pack_return) {
		this.quantity_pack_return = quantity_pack_return;
	}
	public BigInteger getAmount_return() {
		return amount_return;
	}
	public void setAmount_return(BigInteger amount_return) {
		this.amount_return = amount_return;
	}
	public Integer getQuantity_approved() {
		return quantity_approved;
	}
	public void setQuantity_approved(Integer quantity_approved) {
		this.quantity_approved = quantity_approved;
	}
	public Integer getQuantity_retail_approved() {
		return quantity_retail_approved;
	}
	public void setQuantity_retail_approved(Integer quantity_retail_approved) {
		this.quantity_retail_approved = quantity_retail_approved;
	}
	public Integer getQuantity_pack_approved() {
		return quantity_pack_approved;
	}
	public void setQuantity_pack_approved(Integer quantity_pack_approved) {
		this.quantity_pack_approved = quantity_pack_approved;
	}
	public BigInteger getAmount_approved() {
		return amount_approved;
	}
	public void setAmount_approved(BigInteger amount_approved) {
		this.amount_approved = amount_approved;
	}
	public Integer getConvfact() {
		return convfact;
	}
	public void setConvfact(Integer convfact) {
		this.convfact = convfact;
	}
	public String getUom() {
		return uom;
	}
	public void setUom(String uom) {
		this.uom = uom;
	}
	public String getPrice_type() {
		return price_type;
	}
	public void setPrice_type(String price_type) {
		this.price_type = price_type;
	}
	public Integer getIs_promotion() {
		return is_promotion;
	}
	public void setIs_promotion(Integer is_promotion) {
		this.is_promotion = is_promotion;
	}
	public String getTax_num() {
		return tax_num;
	}
	public void setTax_num(String tax_num) {
		this.tax_num = tax_num;
	}
	public Integer getTax_vat() {
		return tax_vat;
	}
	public void setTax_vat(Integer tax_vat) {
		this.tax_vat = tax_vat;
	}
	public BigInteger getTax_amount() {
		return tax_amount;
	}
	public void setTax_amount(BigInteger tax_amount) {
		this.tax_amount = tax_amount;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getCreate_date() {
		return create_date;
	}
	public void setCreate_date(String create_date) {
		this.create_date = create_date;
	}
	public String getCreate_user() {
		return create_user;
	}
	public void setCreate_user(String create_user) {
		this.create_user = create_user;
	}
	public String getUpdate_date() {
		return update_date;
	}
	public void setUpdate_date(String update_date) {
		this.update_date = update_date;
	}
	public String getUpdate_user() {
		return update_user;
	}
	public void setUpdate_user(String update_user) {
		this.update_user = update_user;
	}
	public String getSync_date() {
		return sync_date;
	}
	public void setSync_date(String sync_date) {
		this.sync_date = sync_date;
	}
	public String getSync_user() {
		return sync_user;
	}
	public void setSync_user(String sync_user) {
		this.sync_user = sync_user;
	}
	public String getProduct_info_code() {
		return product_info_code;
	}
	public void setProduct_info_code(String product_info_code) {
		this.product_info_code = product_info_code;
	}
	
}