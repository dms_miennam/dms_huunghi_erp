/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.database.entity;

/**
 *
 * @author datnv5
 */
public class EntityProcessManager {
    String code;
    String dateexcute;
    String idnumber;
    String content;
    Long duringTime;

    public Long getDuringTime() {
        return duringTime;
    }

    public void setDuringTime(Long duringTime) {
        this.duringTime = duringTime;
    }
    
    
    
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDateexcute() {
        return dateexcute;
    }

    public void setDateexcute(String dateexcute) {
        this.dateexcute = dateexcute;
    }

    public String getIdnumber() {
        return idnumber;
    }

    public void setIdnumber(String idnumber) {
        this.idnumber = idnumber;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    
    
}
