package com.viettel.vts.khdn.database.DAO;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.viettel.vts.khdn.database.DAO.common.CommonDataBaseDao;

public class CommonDAO {
	public String getShopIdByCustomerId(String customerId) {
		CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder query = new StringBuilder();
        List<Object> params = new ArrayList<>();
        query.append("select shop_id from shop where shop_code = ?");
        params.add(customerId);
        BigDecimal value = (BigDecimal) cmd.excuteSqlGetValOnConditionListParams(query, params);
        return value.toString();
	}
	
	public String getCatIdByProductGroupId(String productGroupId) {
		CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder query = new StringBuilder();
        List<Object> params = new ArrayList<>();
        query.append("select product_info_id from product_info where product_info_code = ?");
        params.add(productGroupId);
        BigDecimal value = (BigDecimal) cmd.excuteSqlGetValOnConditionListParams(query, params);
        return value.toString();
	}
	
	public String getProductIdByProductIdERP(String productIdERP) {
		CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder query = new StringBuilder();
        List<Object> params = new ArrayList<>();
        query.append("select product_id from product where product_code = ?");
        params.add(productIdERP);
        BigDecimal value = (BigDecimal) cmd.excuteSqlGetValOnConditionListParams(query, params);
        return value.toString();
	}

	public String getPoIdbySaleOrderId(String salesOrderId) {
		CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder query = new StringBuilder();
        List<Object> params = new ArrayList<>();
        query.append("select po_id from po where SO_NUMBER_ERP = ?");
        params.add(salesOrderId);
        BigDecimal value = (BigDecimal) cmd.excuteSqlGetValOnConditionListParams(query, params);
        return value.toString();
	}
}
