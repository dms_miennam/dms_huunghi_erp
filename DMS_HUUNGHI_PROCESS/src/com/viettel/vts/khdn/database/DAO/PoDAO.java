package com.viettel.vts.khdn.database.DAO;

import java.util.ArrayList;
import java.util.List;

import com.viettel.vts.khdn.database.DAO.common.CommonDataBaseDao;
import com.viettel.vts.khdn.database.entity.EntityPo;
import com.viettel.vts.khdn.database.entity.EntityPoDetail;
import com.viettel.vts.khdn.database.entity.EntityPoReturnDetail;

public class PoDAO {
	public EntityPo getPoByCode(String poCode) {
		CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder query = new StringBuilder();
        List<Object> params = new ArrayList<>();
        query.append(" select * ");
        query.append(" from po where  p.po_numner = ?");
        params.add(poCode);
        List<EntityPo> po =  (List<EntityPo>) cmd.excuteSqlGetListObjOnCondition(
                query, params, null, null, EntityPo.class);
        if(po != null){
            return po.get(0);
        }
        return null;
	}
	
	//update Don Po Duyet + list product
	//update PoDuyet
	public boolean updatePo(EntityPo po) {
		CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder sqlUpdate = new StringBuilder();
	            List<Object> arrParams = new ArrayList<>();
	            // update Po, set ngay duyet, 
	            sqlUpdate.append(" update PO set APPROVED_DATE = ?, "
	            		+ "so_number_erp = ?, "
	            		+ "po_date = ?, "
	            		+ "chanel_code = ?, "
	            		+ "delivery_address_code = ?, "
	            		+ "contact_name = ?, "
	            		+ "status = ?,  "
	            		+ "is_sync = ? "
	            		+ "where po_number = ? ");
	            arrParams.add(po.getSo_number_erp());
	            arrParams.add(po.getPo_date());
	            arrParams.add(po.getChanel_code());
	            arrParams.add(po.getDelivery_address_code());
	            arrParams.add(po.getContact_name());
	            arrParams.add(po.getStatus());
	            arrParams.add(po.getIs_sync());
	            arrParams.add(po.getPo_number());
	            return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
	}
	//update PoDetail
	public boolean updatePoDetail(EntityPoDetail poDetail) {            
		CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder sqlUpdate = new StringBuilder();
	            List<Object> arrParams = new ArrayList<>();
	            // update san luong, so luong duyet, tong tien duyet, hang khuyen mai, ghi chu
	            sqlUpdate.append(" update po_detail set "
	            		+ "uom = ?, "
	            		+ "quantity = ?, "
	            		+ "quantity_approved = ?, "
	            		+ "amount_approved = ? ,"
	            		+ "price_type = ? ,"
	            		+ "is_promotion = ? ,"
	            		+ "tax_num = ? ,"
	            		+ "tax_amount = ? ,"
	            		+ "tax_vat = ? ,"
	            		+ "note = ? "
	            		+ "where PO_ID = ? and PRODUCT_ID = ?");
	            arrParams.add(poDetail.getUom());
	            arrParams.add(poDetail.getQuantity());
	            arrParams.add(poDetail.getQuantity_approved());
	            arrParams.add(poDetail.getAmount_approved());
	            arrParams.add(poDetail.getPrice_type());
	            arrParams.add(poDetail.getIs_promotion());
	            arrParams.add(poDetail.getTax_num());
	            arrParams.add(poDetail.getTax_amount());
	            arrParams.add(poDetail.getTax_vat());
	            arrParams.add(poDetail.getNote());
	            arrParams.add(poDetail.getPo_id());
	            arrParams.add(poDetail.getProduct_id());
	            return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
    }

	public EntityPoDetail getPoDetailByProductId(Long poId ,String productId) {
		CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder query = new StringBuilder();
        List<Object> params = new ArrayList<>();
        query.append(" select * ");
        query.append(" from po_detail where PO_ID = ? and PRODUCT_ID = ?");
        params.add(poId);
        params.add(productId);
        List<EntityPoDetail> poDetails =  (List<EntityPoDetail>) cmd.excuteSqlGetListObjOnCondition(
                query, params, null, null, EntityPoDetail.class);
        if(poDetails != null){
            return poDetails.get(0);
        }
        return null;
	}

	public boolean insertPo(EntityPo poInsert) {
		StringBuilder sqlUpdate = new StringBuilder();
        List<Object> arrParams = new ArrayList<>();
        sqlUpdate.append(" insert into po("
        		+ "po_date, "
        		+ "chanel_code, "
        		+ "shop_id, "
        		+ "delivery_address_code, "
        		+ "cat_id, "
        		+ "contact_name, "
        		+ "status, "
        		+ "note, "
        		+ "po_date, "
        		+ "update_date, "
        		+ "is_sync, "
        		+ "tax_num, "
        		+ "tax_vat, "
        		+ "tax_amount, "
        		+ "amount, "
        		+ "quantity, "
        		+ ") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
        arrParams.add(poInsert.getPo_date());
        arrParams.add(poInsert.getChanel_code());
        arrParams.add(poInsert.getShop_id());
        arrParams.add(poInsert.getDelivery_address_code());
        arrParams.add(poInsert.getCat_id());
        arrParams.add(poInsert.getContact_name());
        arrParams.add(poInsert.getStatus());
        arrParams.add(poInsert.getNote());
        arrParams.add(poInsert.getPo_date());
        arrParams.add(poInsert.getUpdate_date());
        arrParams.add(poInsert.getIs_sync());
        arrParams.add(poInsert.getTax_num());
        arrParams.add(poInsert.getTax_vat());
        arrParams.add(poInsert.getTax_amount());
        arrParams.add(poInsert.getAmount());
        arrParams.add(poInsert.getQuantity());
        CommonDataBaseDao cmd = new CommonDataBaseDao();
        return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
		
	}

	public boolean insertPoDetail(EntityPoDetail poDetailInsert) {
		StringBuilder sqlUpdate = new StringBuilder();
        List<Object> arrParams = new ArrayList<>();
        sqlUpdate.append(" insert into po_detail("
        		+ "product_id, "
        		+ "uom, "
        		+ "is_promotion, "
        		+ "price_type, "
        		+ "package_price_erp, "
        		+ "package_quantity, "
        		+ "retail_price_erp, "
        		+ "retail_quantity, "
        		+ "amount, "
        		+ "note,"
        		+ "tax_amount,"
        		+ "tax_num,"
        		+ "tax_vat"
        		+ ") values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
        arrParams.add(poDetailInsert.getProduct_id());
        arrParams.add(poDetailInsert.getUom());
        arrParams.add(poDetailInsert.getIs_promotion());
        arrParams.add(poDetailInsert.getPrice_type());
        arrParams.add(poDetailInsert.getPackage_price_erp());
        arrParams.add(poDetailInsert.getPackage_quantity());
        arrParams.add(poDetailInsert.getRetail_price_erp());
        arrParams.add(poDetailInsert.getRetail_quantity());
        arrParams.add(poDetailInsert.getAmount());
        arrParams.add(poDetailInsert.getNote());
        arrParams.add(poDetailInsert.getTax_amount());
        arrParams.add(poDetailInsert.getTax_num());
        arrParams.add(poDetailInsert.getTax_vat());
        CommonDataBaseDao cmd = new CommonDataBaseDao();
        return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
		
	}
}
