package com.viettel.vts.khdn.database.DAO;

import java.util.ArrayList;
import java.util.List;

import com.viettel.vts.khdn.database.DAO.common.CommonDataBaseDao;
import com.viettel.vts.khdn.database.entity.EntityPoReturn;
import com.viettel.vts.khdn.database.entity.EntityPoReturnDetail;

public class RoDAO {
	
	public static void main(String[] args) {
		RoDAO t = new RoDAO();
		EntityPoReturnDetail ro = t.getRoDetailByProductId("1", "150");
		String s="1";
	}

	public EntityPoReturn getRoByCode(String dmsRefCode) {
			CommonDataBaseDao cmd = new CommonDataBaseDao();
	        StringBuilder query = new StringBuilder();
	        List<Object> params = new ArrayList<>();
	        query.append(" select * ");
	        query.append(" from po_return where  po_return_number = ?");
	        params.add(dmsRefCode);
	        List<EntityPoReturn> ro =  (List<EntityPoReturn>) cmd.excuteSqlGetListObjOnCondition(
	                query, params, null, null, EntityPoReturn.class);
	        if(ro != null){
	            return ro.get(0);
	        }
	        return null;
	}

	public EntityPoReturnDetail getRoDetailByProductId(String po_return_id, String productId) {
		CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder query = new StringBuilder();
        List<Object> params = new ArrayList<>();
        query.append(" select * ");
        query.append(" from po_return_detail where  po_return_id = ? and product_id = ?");
        params.add(po_return_id);
        params.add(productId);
        List<EntityPoReturnDetail> ro =  (List<EntityPoReturnDetail>) cmd.excuteSqlGetListObjOnCondition(
                query, params, null, null, EntityPoReturnDetail.class);
        if(ro != null){
            return ro.get(0);
        }
        return null;
	}

	public boolean updateRoDetail(EntityPoReturnDetail roDetailFilter) {
		CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder sqlUpdate = new StringBuilder();
	            List<Object> arrParams = new ArrayList<>();
	            // update Po, set ngay duyet, 
	            sqlUpdate.append(" update po_return_retail set "
	            		+ "uom = ?, "
	            		+ "quantity_approved = ?, "
	            		+ "is_promotion = ?, "
	            		+ "price_type = ?, "
	            		+ "tax_num = ?, "
	            		+ "tax_vat = ?, "
	            		+ "tax_amount = ? "
	            		+ "where po_return_id = ? and product_id = ? ");
	            arrParams.add(roDetailFilter.getUom());
	            arrParams.add(roDetailFilter.getQuantity_approved());
	            arrParams.add(roDetailFilter.getIs_promotion());
	            arrParams.add(roDetailFilter.getPrice_type());
	            arrParams.add(roDetailFilter.getTax_num());
	            arrParams.add(roDetailFilter.getTax_vat());
	            arrParams.add(roDetailFilter.getTax_amount());
	            arrParams.add(roDetailFilter.getPo_return_id());
	            arrParams.add(roDetailFilter.getProduct_id());
	            
	            return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
	}

	public boolean updateRo(EntityPoReturn roFilter) {
		CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder sqlUpdate = new StringBuilder();
	            List<Object> arrParams = new ArrayList<>();
	            // update Po, set ngay duyet, 
	            sqlUpdate.append(" update po_return set "
	            		+ "do_date = ?, "
	            		+ "po_return_notifi = ?, "
	            		+ "channel_code = ?, "
	            		+ "shop_id = ?, "
	            		+ "delivery_address_code = ?, "
	            		+ "contact_name = ?, "
	            		+ "status = ?, "
	            		+ "note = ?, "
	            		+ "create_date = ?, "
	            		+ "sync_date = ?, "
	            		+ "is_sync = ?, "
	            		+ "quantity_return = ?, "
	            		+ "amount_return = ?, "
	            		+ "delivery_date = ?, "
	            		+ "ship_date = ?, "
	            		+ "tax_num = ?, "
	            		+ "tax_vat = ?, "
	            		+ "tax_amount = ?, "
	            		+ "total_return = ?, "
	            		+ "where po_return_number = ?");
	            arrParams.add(roFilter.getDo_date());
	            arrParams.add(roFilter.getPo_return_notifi());
	            arrParams.add(roFilter.getChannel_code());
	            arrParams.add(roFilter.getShop_id());
	            arrParams.add(roFilter.getDelivery_address_code());
	            arrParams.add(roFilter.getContact_name());
	            arrParams.add(roFilter.getStatus());
	            arrParams.add(roFilter.getNote());
	            arrParams.add(roFilter.getCreate_date());
	            arrParams.add(roFilter.getSync_date());
	            arrParams.add(roFilter.getIs_sync());
	            arrParams.add(roFilter.getQuantity_return());
	            arrParams.add(roFilter.getAmount_return());
	            arrParams.add(roFilter.getDelivery_date());
	            arrParams.add(roFilter.getShip_date());
	            arrParams.add(roFilter.getTax_num());
	            arrParams.add(roFilter.getTax_vat());
	            arrParams.add(roFilter.getTax_amount());
	            arrParams.add(roFilter.getTotal_return());
	            arrParams.add(roFilter.getPo_return_number());
	            
	            return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
	}

}
