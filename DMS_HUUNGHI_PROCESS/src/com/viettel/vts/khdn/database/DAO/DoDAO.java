package com.viettel.vts.khdn.database.DAO;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import com.viettel.vts.khdn.constants.FunctionCommon;
import com.viettel.vts.khdn.database.DAO.common.CommonDataBaseDao;
import com.viettel.vts.khdn.database.entity.EntityDo;
import com.viettel.vts.khdn.database.entity.EntityDoDetail;
import com.viettel.vts.khdn.database.entity.EntityPoDetail;

public class DoDAO {
		public EntityDo getDoByCode(String doId) {
			CommonDataBaseDao cmd = new CommonDataBaseDao();
	        StringBuilder query = new StringBuilder();
	        List<Object> params = new ArrayList<>();
	        query.append(" select * ");
	        query.append(" from do where  do_id = ?");
	        params.add(doId);
	        List<EntityDo> entityDos =  (List<EntityDo>) cmd.excuteSqlGetListObjOnCondition(
	                query, params, null, null, EntityDo.class);
	        if(entityDos != null){
	            return entityDos.get(0);
	        }
	        return null;
	}

		public boolean insertDoDetail(EntityDoDetail doDetail) {
			 StringBuilder sqlUpdate = new StringBuilder();
		        List<Object> arrParams = new ArrayList<>();
		        sqlUpdate.append(" insert into do_detail("
		        		+ "product_id, "
		        		+ "uom, "
		        		+ "is_promotion, "
		        		+ "price_type, "
		        		+ "package_price, "
		        		+ "quantity_pack_offer_export, "
		        		+ "retail_price, "
		        		+ "quantity_retail_offer_export, "
		        		+ "amount_offer_export, "
		        		+ "note ,"
		        		+ "tax_amount ,"
		        		+ "tax_num ,"
		        		+ "tax_vat "
		        		+ ") values(?,?,?,?,?,?,?,?,?,?,?,?,?)");
		        arrParams.add(doDetail.getProduct_id());
		        arrParams.add(doDetail.getUom());
		        arrParams.add(doDetail.getIs_promotion());
		        arrParams.add(doDetail.getPrice_type());
		        arrParams.add(doDetail.getPackage_price());
		        arrParams.add(doDetail.getQuantity_pack_offer_export());
		        arrParams.add(doDetail.getRetail_price());
		        arrParams.add(doDetail.getQuantity_retail_offer_export());
		        arrParams.add(doDetail.getAmount_offer_export());
		        arrParams.add(doDetail.getNote());
		        arrParams.add(doDetail.getTax_amount());
		        arrParams.add(doDetail.getTax_num());
		        arrParams.add(doDetail.getTax_vat());
		        CommonDataBaseDao cmd = new CommonDataBaseDao();
		        return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
		}

		public boolean insertDo(EntityDo doInsert) {
			StringBuilder sqlUpdate = new StringBuilder();
	        List<Object> arrParams = new ArrayList<>();
	        sqlUpdate.append(" insert into do("
	        		+ "do_number, "
	        		+ "do_date, "
	        		+ "channel_code, "
	        		+ "shop_id, "
	        		+ "delivery_address_code, "
	        		+ "cat_id, "
	        		+ "contact_name, "
	        		+ "status, "
	        		+ "note, "
	        		+ "do_date, "
	        		+ "update_date, "
	        		+ "is_sync, "
	        		+ "tax_num, "
	        		+ "tax_vat, "
	        		+ "tax_amount, "
	        		+ "amount_offer_export, "
	        		+ "quantity_offer_export, "
	        		+ ") values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");
	        arrParams.add(doInsert.getDo_number());
	        arrParams.add(doInsert.getDo_date());
	        arrParams.add(doInsert.getChannel_code());
	        arrParams.add(doInsert.getShop_id());
	        arrParams.add(doInsert.getDelivery_address_code());
	        arrParams.add(doInsert.getCat_id());
	        arrParams.add(doInsert.getContact_name());
	        arrParams.add(doInsert.getStatus());
	        arrParams.add(doInsert.getNote());
	        arrParams.add(doInsert.getDo_date());
	        arrParams.add(doInsert.getUpdate_date());
	        arrParams.add(doInsert.getIs_sync());
	        arrParams.add(doInsert.getTax_num());
	        arrParams.add(doInsert.getTax_vat());
	        arrParams.add(doInsert.getTax_amount());
	        arrParams.add(doInsert.getAmount_offer_export());
	        arrParams.add(doInsert.getQuantity_offer_export());
	        CommonDataBaseDao cmd = new CommonDataBaseDao();
	        return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
		}

		public EntityDoDetail getDoDetailByDoId(Long do_id, String productId) {
			CommonDataBaseDao cmd = new CommonDataBaseDao();
	        StringBuilder query = new StringBuilder();
	        List<Object> params = new ArrayList<>();
	        query.append(" select * ");
	        query.append(" from do_detail where  do_id = ? and product_id = ?");
	        params.add(do_id);
	        params.add(productId);
	        List<EntityDoDetail> entityDos =  (List<EntityDoDetail>) cmd.excuteSqlGetListObjOnCondition(
	                query, params, null, null, EntityDoDetail.class);
	        if(entityDos != null){
	            return entityDos.get(0);
	        }
	        return null;
		}

		public boolean updateDoDetail(EntityDoDetail doDetailFilter) {
			CommonDataBaseDao cmd = new CommonDataBaseDao();
	        StringBuilder sqlUpdate = new StringBuilder();
		            List<Object> arrParams = new ArrayList<>();
		            // update Po, set ngay duyet, 
		            sqlUpdate.append(" update do_detail set "
		            		+ "product_id = ?, "
		            		+ "uom = ?, "
		            		+ "price_type = ?, "
		            		+ "package_price = ?, "
		            		+ "quantity_real_export = ? "
		            		+ "where DO_DETAIL_ID = ?");
		            arrParams.add(doDetailFilter.getProduct_id());
		            arrParams.add(doDetailFilter.getUom());
		            arrParams.add(doDetailFilter.getPrice_type());
		            arrParams.add(doDetailFilter.getPackage_price());
		            arrParams.add(doDetailFilter.getQuantity_real_export());
		            arrParams.add(doDetailFilter.getDo_detail_id());
		            return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
			
		}

		public boolean updateDo(EntityDo doFilter) {
			CommonDataBaseDao cmd = new CommonDataBaseDao();
	        StringBuilder sqlUpdate = new StringBuilder();
		            List<Object> arrParams = new ArrayList<>();
		            // update Po, set ngay duyet, 
		            sqlUpdate.append(" update do set "
		            		+ "issue_code = ?, "
		            		+ "issue_date = ?, "
		            		+ "channel_code = ?, "
		            		+ "shop_id = ?, "
		            		+ "delivery_address_code = ?, "
		            		+ "cat_id = ?, "
		            		+ "contact_name = ?, "
		            		+ "status = ?, "
		            		+ "note = ?, "
		            		+ "update_date = ?, "
		            		+ "sync_date = ?, "
		            		+ "is_sync = ?, "
		            		+ "invoice_number = ?, "
		            		+ "invoice_date = ?, "
		            		+ "quantity_real_export = ? "
		            		+ "where do_id = ?");
		            arrParams.add(doFilter.getIssue_code());
		            arrParams.add(doFilter.getIssue_date());
		            arrParams.add(doFilter.getChannel_code());
		            arrParams.add(doFilter.getShop_id());
		            arrParams.add(doFilter.getDelivery_address_code());
		            arrParams.add(doFilter.getCat_id());
		            arrParams.add(doFilter.getContact_name());
		            arrParams.add(doFilter.getStatus());
		            arrParams.add(doFilter.getNote());
		            arrParams.add(doFilter.getUpdate_date());
		            arrParams.add(doFilter.getSync_date());
		            arrParams.add(doFilter.getIs_sync());
		            arrParams.add(doFilter.getInvoice_number());
		            arrParams.add(doFilter.getInvoice_number());
		            arrParams.add(doFilter.getQuantity_real_export());
		            return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
			
		}
		
//		public static void main(String[] args) {
//			DoDAO dod= new DoDAO();
//			String s = dod.getProductIdByProductIdERP("3.1.010.002");
//			System.out.println(s);
//		}
}
