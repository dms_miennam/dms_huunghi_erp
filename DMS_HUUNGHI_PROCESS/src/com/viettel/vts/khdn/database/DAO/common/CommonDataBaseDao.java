/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.database.DAO.common;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonObject;
import com.viettel.vts.khdn.constants.FunctionCommon;
import com.viettel.vts.khdn.constants.StringConstants;
import com.viettel.vts.khdn.database.connect.BaseDataDao;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import org.apache.log4j.Logger;

/**
 * Thuc hien thao tac du lieu tren voffice 1.0
 *
 * @author datnv5
 */
public class CommonDataBaseDao extends BaseDataDao {

    private static final Logger LOGS = Logger.getLogger(CommonDataBaseDao.class);
    //==========================thuc hien lay du lieu==========================

    /**
     * 1. Thuc hien query lay ra bang du lieu theo dieu kien
     *
     * @param queryString : cau lenh sql
     * @param arrParams: hashmap dieu kien
     * @param startPage
     * @param pageLoad
     * @param classOfT
     * @return Object: trả về đối tượng theo class đẩy vào
     */
    public List<? extends Object> excuteSqlGetListObjOnCondition(StringBuilder queryString,
            List<Object> arrParams, Long startPage, Long pageLoad, Class<?> classOfT) {
        List<Object> resultObj = new ArrayList<>();
        ResultSet rs = null;
        try {
            Field fieldlist[] = classOfT.getDeclaredFields();
            //Field fieldlist[] = classOfT.getClass().getDeclaredFields();
            StringBuilder sqlPage = new StringBuilder();
            if (startPage != null && pageLoad != null) {
                sqlPage.append(" SELECT /*+ FIRST_ROWS(20) */ * FROM ( SELECT a.*, rownum r__  FROM (");
                sqlPage.append(queryString.toString());
                sqlPage.append(String.format(" ) a WHERE rownum <= %d) ", startPage + pageLoad));
                sqlPage.append(String.format(" WHERE r__ > %d", startPage));
            } else {
                sqlPage = queryString;
            }

            conn = openConnection();
            PreparedStatement updateSqlStatement = conn.prepareStatement(sqlPage.toString());

            if (arrParams != null && arrParams.size() > 0) {
                //add dieu kien vao cau lenh sql
                for (int i = 0; i < arrParams.size(); i++) {
                    Object object = arrParams.get(i);
                    updateSqlStatement.setObject(i + 1, object);
                }
            }
            rs = updateSqlStatement.executeQuery();

            while (rs.next()) {
                JsonObject datasetItem = new JsonObject();
                for (Field field : fieldlist) {
                    //thuc hien add du lieu vao json
                    String fileName = field.getName();
                    if (FunctionCommon.hasColumn(rs, fileName)) {
                        datasetItem.addProperty(fileName, rs.getString(fileName));
                    }
                }
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                Object item = gson.fromJson(datasetItem, classOfT);
                resultObj.add(item);
            }

        } catch (SQLException ex2) {
            LOGS.error("Loi! excuteSqlOnCondition: ", ex2);
            System.out.println(ex2);
        } finally {
            closeConnection();
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOGS.error("Loi! SQLException: ", ex);
                }
            }
        }
        return resultObj;
    }

    /**
     * 1.2. Thuc hien query lay ra bang du lieu theo dieu kien
     *
     * @param queryString : cau lenh sql
     * @param arrMapParams
     * @param startPage
     * @param pageLoad
     * @param classOfT
     * @return Object: trả về đối tượng theo class đẩy vào
     */
    public List<? extends Object> excuteGetDataOnCondition(StringBuilder queryString,
            HashMap<String, Object> arrMapParams, Long startPage, Long pageLoad, Class<?> classOfT) {
        List<Object> resultObj = new ArrayList<>();
        ResultSet rs = null;
        try {
            Field fieldlist[] = classOfT.getDeclaredFields();
            StringBuilder sqlPage = new StringBuilder();
            if (startPage != null && pageLoad != null) {
                sqlPage.append(" SELECT /*+ FIRST_ROWS(20) */ * FROM ( SELECT a.*, rownum r__  FROM (");
                sqlPage.append(queryString.toString());
                sqlPage.append(String.format(" ) a WHERE rownum <= %d) ", startPage + pageLoad));
                sqlPage.append(String.format(" WHERE r__ > %d", startPage));
            } else {
                sqlPage = queryString;
            }

            //sql chuan hoa day vao query
            String sqlQuery = FunctionCommon.trimspace(sqlPage.toString());
            List<Object> paramsFinal = new ArrayList<>();
            if (arrMapParams != null) {
                //sap xep key thay the theo thu tu
                List<String> resultKey = getOrderKeyParamsSql(sqlQuery, arrMapParams);
                for (int k = 0; k < resultKey.size(); ++k) {
                    String keyReplace = String.format(StringConstants.STR_SQLPARAM, resultKey.get(k)).trim();
                    Object objectParams = arrMapParams.get(resultKey.get(k));
                    if (objectParams.getClass() == ArrayList.class) {
                        //la doi tuong dang mang kieu in
                        ArrayList objectParamsArr = (ArrayList) objectParams;
                        String sqlRepLaceNew = "";
                        if (objectParamsArr.size() > 800) {
                            int[] goldSplit = FunctionCommon.getReplaceSqlInArr(sqlQuery, keyReplace);
                            int startReplace = goldSplit[0];
                            int endColumn = goldSplit[1];
                            int endReplace = goldSplit[2];
                            int intInOrNotin = goldSplit[3];
                            String strReplace = sqlQuery.substring(startReplace, endReplace);
                            String strColumn = sqlQuery.substring(startReplace, endColumn);
                            String strInOrNotIn = sqlQuery.substring(endColumn, intInOrNotin);
                            //gan la lon nhat in 800 phan tu thi se tach
                            int countSplit = objectParamsArr.size() / 800 + 1;
                            sqlRepLaceNew = "(1=2 ";
                            for (int i = 0; i < countSplit; i++) {
                                if (i * 800 < objectParamsArr.size()) {
                                    sqlRepLaceNew += " OR " + strColumn + " " + strInOrNotIn + "(";
                                    String strInItem = "";
                                    for (int j = i * 800; j < objectParamsArr.size(); j++) {
                                        strInItem += StringConstants.STR_SQLASKPARAM + ",";
                                        paramsFinal.add(objectParamsArr.get(j));
                                    }
                                    if (strInItem.trim().length() > 0) {
                                        strInItem = strInItem.substring(0, strInItem.length() - 1);
                                    }
                                    sqlRepLaceNew += strInItem + " ) ";
                                }
                            }
                            sqlRepLaceNew += " ) ";
                            sqlQuery = sqlQuery.replace(strReplace, sqlRepLaceNew);
                        } else {
                            //ban ghi in co so phan tu it hon 800
                            for (int i = 0; i < objectParamsArr.size(); i++) {
                                //gep chuoi thay the moi
                                sqlRepLaceNew += StringConstants.STR_SQLASKPARAM + ",";
                                //add bien kem theo
                                paramsFinal.add(objectParamsArr.get(i));
                            }
                            //cat di ky tu thu ở cuối
                            if (sqlRepLaceNew.trim().length() > 0) {
                                sqlRepLaceNew = sqlRepLaceNew.substring(0, sqlRepLaceNew.length() - 1);
                            }
                            //thay the vao sql chinh
                            sqlQuery = sqlQuery.replace(keyReplace, sqlRepLaceNew);
                        }

                    } else {
                        //params thay the dạng thuong
                        sqlQuery = sqlQuery.replace(keyReplace, StringConstants.STR_SQLASKPARAM);
                        paramsFinal.add(objectParams);
                    }
                }
            }

            conn = openConnection();
            PreparedStatement updateSqlStatement = conn.prepareStatement(sqlQuery);

            if (paramsFinal.size() > 0) {
                //add dieu kien vao cau lenh sql
                for (int i = 0; i < paramsFinal.size(); i++) {
                    Object object = paramsFinal.get(i);
                    if (object.getClass() == String.class) {
                        String strLike = (String) object;
                        strLike = FunctionCommon.escapeSql(strLike);
                        updateSqlStatement.setString(i + 1, strLike);
                    } else {
                        updateSqlStatement.setObject(i + 1, object);
                    }

                }
            }

            rs = updateSqlStatement.executeQuery();

            while (rs.next()) {
                JsonObject datasetItem = new JsonObject();
                for (Field field : fieldlist) {
                    //thuc hien add du lieu vao json
                    String fileName = field.getName();
                    if (FunctionCommon.hasColumn(rs, fileName)) {
                        datasetItem.addProperty(fileName, String.valueOf(rs.getObject(fileName)));
                    }
                }
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();
                Object item = gson.fromJson(datasetItem, classOfT);
                resultObj.add(item);
            }

        } catch (SQLException | IllegalArgumentException | SecurityException ex2) {
            LOGS.error("Loi! excuteSqlOnCondition: ", ex2);
        } finally {
            closeConnection();
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOGS.error("Loi! SQLException: ", ex);
                }
            }
        }
        return resultObj;
    }

    /**
     * 2. lay so luong ban ghi theo sql
     *
     * @param queryString
     * @param arrMapParams
     * @return
     */
    public Long excuteSqlGetCountOnCondition(StringBuilder queryString,
            HashMap<String, Object> arrMapParams) {
        Long resultValue = null;
        ResultSet rs = null;
        try {
            StringBuilder sqlPage = new StringBuilder();

            sqlPage.append("Select count(*) as count From (");
            sqlPage.append(queryString.toString());
            sqlPage.append(")");

            //sql chuan hoa day vao query
            String sqlQuery = FunctionCommon.trimspace(sqlPage.toString());
            List<Object> paramsFinal = new ArrayList<>();
            if (arrMapParams != null) {
                //sap xep key thay the theo thu tu
                List<String> resultKey = getOrderKeyParamsSql(sqlQuery, arrMapParams);
                for (int k = 0; k < resultKey.size(); ++k) {
                    String keyReplace = String.format(StringConstants.STR_SQLPARAM, resultKey.get(k)).trim();
                    Object objectParams = arrMapParams.get(resultKey.get(k));
                    if (objectParams.getClass() == ArrayList.class) {
                        //la doi tuong dang mang kieu in
                        ArrayList objectParamsArr = (ArrayList) objectParams;
                        String sqlRepLaceNew = "";
                        if (objectParamsArr.size() > 800) {
                            int[] goldSplit = FunctionCommon.getReplaceSqlInArr(sqlQuery, keyReplace);
                            int startReplace = goldSplit[0];
                            int endColumn = goldSplit[1];
                            int endReplace = goldSplit[2];
                            int intInOrNotin = goldSplit[3];
                            String strReplace = sqlQuery.substring(startReplace, endReplace);
                            String strColumn = sqlQuery.substring(startReplace, endColumn);
                            String strInOrNotIn = sqlQuery.substring(endColumn, intInOrNotin);
                            //gan la lon nhat in 800 phan tu thi se tach
                            int countSplit = objectParamsArr.size() / 800 + 1;
                            sqlRepLaceNew = "(1=2 ";
                            for (int i = 0; i < countSplit; i++) {
                                if (i * 800 < objectParamsArr.size()) {
                                    sqlRepLaceNew += " OR " + strColumn + " " + strInOrNotIn + "(";
                                    String strInItem = "";
                                    for (int j = i * 800; j < objectParamsArr.size(); j++) {
                                        strInItem += StringConstants.STR_SQLASKPARAM + ",";
                                        paramsFinal.add(objectParamsArr.get(j));
                                    }
                                    if (strInItem.trim().length() > 0) {
                                        strInItem = strInItem.substring(0, strInItem.length() - 1);
                                    }
                                    sqlRepLaceNew += strInItem + " ) ";
                                }
                            }
                            sqlRepLaceNew += " ) ";
                            sqlQuery = sqlQuery.replace(strReplace, sqlRepLaceNew);
                        } else {
                            //ban ghi in co so phan tu it hon 800
                            for (int i = 0; i < objectParamsArr.size(); i++) {
                                //gep chuoi thay the moi
                                sqlRepLaceNew += StringConstants.STR_SQLASKPARAM + ",";
                                //add bien kem theo
                                paramsFinal.add(objectParamsArr.get(i));
                            }
                            //cat di ky tu thu ở cuối
                            if (sqlRepLaceNew.trim().length() > 0) {
                                sqlRepLaceNew = sqlRepLaceNew.substring(0, sqlRepLaceNew.length() - 1);
                            }
                            //thay the vao sql chinh
                            sqlQuery = sqlQuery.replace(keyReplace, sqlRepLaceNew);
                        }

                    } else {
                        //params thay the dạng thuong
                        sqlQuery = sqlQuery.replace(keyReplace, StringConstants.STR_SQLASKPARAM);
                        paramsFinal.add(objectParams);
                    }
                }
            }

            conn = openConnection();
            PreparedStatement updateSqlStatement = conn.prepareStatement(sqlQuery);

            if (paramsFinal.size() > 0) {
                //add dieu kien vao cau lenh sql
                for (int i = 0; i < paramsFinal.size(); i++) {
                    Object object = paramsFinal.get(i);
                    if (object.getClass() == String.class) {
                        String strLike = (String) object;
                        strLike = FunctionCommon.escapeSql(strLike);
                        updateSqlStatement.setString(i + 1, strLike);
                    } else {
                        updateSqlStatement.setObject(i + 1, object);
                    }

                }
            }

            rs = updateSqlStatement.executeQuery();
            while (rs.next()) {
                resultValue = rs.getLong(1);
            }
        } catch (SQLException ex1) {
            LOGS.error("Loi! excuteSqlGetCountOnCondition: ", ex1);
        } finally {
            closeConnection();
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOGS.error("Loi! SQLException: ", ex);
                }
            }
        }
        return resultValue;
    }

    //=================thuc hien update chen xoa du lieu========================
    /**
     * thuc hien chen hoa update du lieu vao data base
     *
     * @param sqlInsert
     * @param arrMapParams
     * @return
     */
    public Boolean insertOrUpdateDataBase(StringBuilder sqlInsert, HashMap<String, Object> arrMapParams) {
        Boolean valueResult = false;
        if (sqlInsert != null && sqlInsert.toString().trim().length() > 0) {
            try {
                conn = openConnection();
                String sqlExcute = sqlInsert.toString();

                //thuc hien sap xep key tu khoa theo thu tu
                if (arrMapParams != null && arrMapParams.size() > 0) {
                    List<String> resultKey = getOrderKeyParamsSql(sqlInsert.toString(), arrMapParams);
                    for (int i = 0; i < resultKey.size(); i++) {
                        String keyReplace = String.format(StringConstants.STR_SQLPARAM,
                                resultKey.get(i)).trim();
                        //lay gia tri can chen theo tu khoa
                        sqlExcute = sqlExcute.replace(keyReplace, StringConstants.STR_SQLASKPARAM);
                    }
                    PreparedStatement excuteSqlStatement = conn.prepareStatement(sqlExcute);
                    for (int i = 0; i < resultKey.size(); i++) {
                        Object object = arrMapParams.get(resultKey.get(i));
                        excuteSqlStatement.setObject(i + 1, object);
                    }
                    int resultInsert = excuteSqlStatement.executeUpdate();
                    valueResult = resultInsert >= 0;
                }
            } catch (SQLException e) {
                LOGS.error("Loi! Khong chen duoc ban ghi: ", e);
                valueResult = false;
            } finally {
                closeConnection();
            }
        } else {
            valueResult = false;
        }
        return valueResult;
    }

    /**
     * thuc hien chen hoa update du lieu vao data base
     *
     * @param queryString
     * @param arrParams
     * @return
     */
    public Boolean insertOrUpdateDataBase(StringBuilder queryString,
            List<Object> arrParams) {
        Boolean valueResult = false;
        try {
            conn = openConnection();
            PreparedStatement updateSqlStatement = conn.prepareStatement(queryString.toString());

            if (arrParams != null && arrParams.size() > 0) {
                //add dieu kien vao cau lenh sql
                for (int i = 0; i < arrParams.size(); i++) {
                    Object object = arrParams.get(i);
                    updateSqlStatement.setObject(i + 1, object);
                }
            }
            int resultInsert = updateSqlStatement.executeUpdate();
            valueResult = resultInsert >= 0;
        } catch (SQLException | IllegalArgumentException | SecurityException ex2) {
            LOGS.error("Loi! excuteSqlOnCondition: ", ex2);
        } finally {
            closeConnection();
        }
        return valueResult;
    }

    /**
     * thuc hien chen du lieu theo lo vao data base
     *
     * @param sqlInsert
     * @param listArrMapParams
     * @return
     */
    public Boolean insertDataBaseBloc(StringBuilder sqlInsert, List<HashMap<String, Object>> listArrMapParams) {
        Boolean valueResult = false;

        if (sqlInsert != null && sqlInsert.toString().trim().length() > 0) {
            try {
                conn = openConnection();
                String sqlExcute = sqlInsert.toString();

                //thuc hien sap xep key tu khoa theo thu tu
                if (listArrMapParams != null && listArrMapParams.size() > 0) {
                    HashMap<String, Object> arrMapParams = listArrMapParams.get(0);
                    List<String> resultKey = getOrderKeyParamsSql(sqlInsert.toString(), arrMapParams);
                    for (int i = 0; i < resultKey.size(); i++) {
                        String keyReplace = String.format(StringConstants.STR_SQLPARAM,
                                resultKey.get(i)).trim();
                        //lay gia tri can chen theo tu khoa
                        sqlExcute = sqlExcute.replace(keyReplace, StringConstants.STR_SQLASKPARAM);
                    }
                    PreparedStatement excuteSqlStatement = conn.prepareStatement(sqlExcute);
                    for (int j = 0; j < listArrMapParams.size(); j++) {
                        HashMap<String, Object> itemArrMapParams = listArrMapParams.get(j);
                        for (int i = 0; i < resultKey.size(); i++) {
                            Object object = itemArrMapParams.get(resultKey.get(i));
                            excuteSqlStatement.setObject(i + 1, object);
                        }
                        excuteSqlStatement.addBatch();
                    }

                    int[] resultInsert = excuteSqlStatement.executeBatch();
                    excuteSqlStatement.close();
                    conn.commit();
                    valueResult = true;
                    for (int i = 0; i < resultInsert.length; i++) {
                        if (resultInsert[i] < 0 && resultInsert[i] != -2) {
                            valueResult = false;
                            break;
                        }
                    }
                }
            } catch (SQLException e) {
                try {
                    LOGS.error("Loi! Khong chen duoc ban ghi: ", e);
                    conn.rollback();
                    valueResult = false;
                } catch (SQLException ex) {
                    LOGS.error("Loi! SQLException: ", ex);
                }
            } finally {
                closeConnection();
            }
        } else {
            valueResult = false;
        }
        return valueResult;
    }

    /**
     * thuc hien xoa ban ghi theo dataBase
     *
     * @param sqlDelete
     * @param arrMapParams
     * @return
     */
    public Boolean deleteDataBase(StringBuilder sqlDelete, HashMap<String, Object> arrMapParams) {
        Boolean result = false;
        try {
            conn = openConnection();
            String sqlExcute = sqlDelete.toString();
            List<String> resultKey = getOrderKeyParamsSql(sqlExcute, arrMapParams);
            if (arrMapParams != null) {
                for (int i = 0; i < resultKey.size(); i++) {
                    String keyReplace = String.format(StringConstants.STR_SQLPARAM,
                            resultKey.get(i)).trim();
                    //lay gia tri can chen theo tu khoa
                    sqlExcute = sqlExcute.replace(keyReplace, StringConstants.STR_SQLASKPARAM);
                }
                PreparedStatement excuteSqlStatement = conn.prepareStatement(sqlExcute);

                for (int i = 0; i < resultKey.size(); i++) {
                    Object object = arrMapParams.get(resultKey.get(i));
                    excuteSqlStatement.setObject(i + 1, object);
                }
                int valueexcute = excuteSqlStatement.executeUpdate();
                result = valueexcute >= 0;
            }
            //add dieu kien vao
        } catch (SQLException e) {
            result = false;
            LOGS.error("Loi! Khong xoa duoc database ", e);
        }
        return result;
    }

    /**
     * Thực hiện lấy ra thứ tự các key trong chuỗi sql để thay thế biến
     *
     * @param sqlQuery
     * @param arrMapParams
     * @return
     */
    private List<String> getOrderKeyParamsSql(String sqlQuery,
            HashMap<String, Object> arrMapParams) {
        List<String> result = new ArrayList<>();
        HashMap<Integer, Object> resultHmap = new HashMap<>();
        if (arrMapParams != null) {
            for (Entry<String, Object> entry : arrMapParams.entrySet()) {
                String key = String.format(StringConstants.STR_SQLPARAM, entry.getKey().trim());
                int indexOfParams = sqlQuery.indexOf(key);
                resultHmap.put(indexOfParams, entry.getKey().trim());
            }
            result = FunctionCommon.sortHmap(resultHmap);
        }
        return result;
    }

    /**
     * Ham cu thuc hien dem count
     *
     * @param queryString
     * @param arrParams
     * @return
     */
    public Long excuteSqlGetCountOnConditionListParams(StringBuilder queryString,
            List<Object> arrParams) {
        Long resultValue = null;
        ResultSet rs = null;
        try {
            StringBuilder strBuild = new StringBuilder();
            strBuild.append("Select count(*) as count From (");
            strBuild.append(queryString);
            strBuild.append(")");

            conn = openConnection();
            PreparedStatement updateSqlStatement = conn.prepareStatement(strBuild.toString());
            if (arrParams != null && arrParams.size() > 0) {
                //add dieu kien vao cau lenh sql
                for (int i = 0; i < arrParams.size(); i++) {
                    Object object = arrParams.get(i);
                    updateSqlStatement.setObject(i + 1, object);
                }
            }
            rs = updateSqlStatement.executeQuery();
            while (rs.next()) {
                resultValue = rs.getLong(1);
            }
        } catch (SQLException ex1) {
            LOGS.error("Loi! excuteSqlGetCountOnCondition: ", ex1);
        } finally {
            closeConnection();
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOGS.error("Loi! SQLException: ", ex);
                }
            }
        }
        return resultValue;
    }

    /**
     * Ham cu thuc hien lay gia tri trong Db
     *
     * @param queryString
     * @param arrParams
     * @return
     */
    public Object excuteSqlGetValOnConditionListParams(StringBuilder queryString,
            List<Object> arrParams) {
        Object resultValue = null;
        ResultSet rs = null;
        try {

            conn = openConnection();
            PreparedStatement updateSqlStatement = conn.prepareStatement(queryString.toString());
            if (arrParams != null && arrParams.size() > 0) {
                //add dieu kien vao cau lenh sql
                for (int i = 0; i < arrParams.size(); i++) {
                    Object object = arrParams.get(i);
                    updateSqlStatement.setObject(i + 1, object);
                }
            }
            rs = updateSqlStatement.executeQuery();
            while (rs.next()) {
                resultValue = rs.getObject(1);
                break;
            }
        } catch (SQLException ex1) {
            LOGS.error("Loi! excuteSqlGetValOnConditionListParams: ", ex1);
        } finally {
            closeConnection();
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    LOGS.error("Loi! excuteSqlGetValOnConditionListParams: ", ex);
                }
            }
        }
        return resultValue;
    }

    /**
     * <b>Chen hoac cap nhat du lieu theo bloc</b><br>
     *
     * @param sqlInsert
     * @param listParams
     * @return
     */
    public synchronized Boolean insertDataByBloc(StringBuilder sqlInsert,
            List<List<Object>> listParams) {
        Boolean valueResult = false;
        if (sqlInsert != null && sqlInsert.toString().trim().length() > 0) {
            try {
                conn = openConnection();
                String sqlExcute = sqlInsert.toString();
                if (listParams != null && listParams.size() > 0) {
                    int[] resultInsert;
                    try (PreparedStatement excuteSqlStatement = conn.prepareStatement(sqlExcute)) {
                        for (int j = 0; j < listParams.size(); j++) {
                            List<Object> listChidlParams = listParams.get(j);
                            for (int i = 0; i < listChidlParams.size(); i++) {
                                excuteSqlStatement.setObject(i + 1, listChidlParams.get(i));
                            }
                            excuteSqlStatement.addBatch();
                        }
                        excuteSqlStatement.setQueryTimeout(360);
                        resultInsert = excuteSqlStatement.executeBatch();
                    }
                    conn.commit();
                    valueResult = true;
                    for (int i = 0; i < resultInsert.length; i++) {
                        if (resultInsert[i] < 0 && resultInsert[i] != -2) {
                            valueResult = false;
                            break;
                        }
                    }
                }
            } catch (SQLException e) {
                try {
                    LOGS.error("excuteSqlGetValOnConditionListParams(insertDataByBloc) - "
                            + "Exception query: " + sqlInsert + "\nparams: " + listParams, e);
                    LOGS.error(e);
                    conn.rollback();
                    valueResult = false;
                } catch (SQLException ex) {
                    LOGS.error("excuteSqlGetValOnConditionListParams(RoleBack) - "
                            + "Exception query: " + sqlInsert + "\nparams: " + listParams, e);
                    LOGS.error(ex);
                }
            } finally {
                closeConnection();
            }
        }
        return valueResult;
    }
}
