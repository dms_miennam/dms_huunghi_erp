/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.database.DAO;

import com.viettel.vts.khdn.constants.FunctionCommon;
import com.viettel.vts.khdn.database.DAO.common.CommonDataBaseDao;
import com.viettel.vts.khdn.database.entity.EntityProcessManager;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author datnv5
 */
public class ProcessManagerDAO {

    public static final long INDEXING_USER = 1L;

    /**
     * thuc hien lay gia tri trong bang cau hinh
     *
     * @param strKey
     * @return
     */
    public String getValueFromConfigDataBase(String strKey) {
        CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder query = new StringBuilder();
        List<Object> params = new ArrayList<>();
        query.append("select VALUE from system_parameter where CODE = ?");
        params.add(strKey);
        String value = (String) cmd.excuteSqlGetValOnConditionListParams(query, params);
        return value;
    }

    /**
     * Lay thong tin keyProcessRun
     *
     * @param strKey
     * @return
     */
    public EntityProcessManager getSystemProcessmanager(String strKey) {
        CommonDataBaseDao cmd = new CommonDataBaseDao();
        StringBuilder query = new StringBuilder();
        List<Object> params = new ArrayList<>();
        query.append(" select code,to_char(dateexcute,'yyyy-MM-dd') dateexcute, ");
        query.append(" round((sysdate - dateexcute)*24 *60*60,0) as duringTime, idnumber,content from system_process_manager where code = ?");
        params.add(strKey);
        List<EntityProcessManager> value =  (List<EntityProcessManager>) cmd.excuteSqlGetListObjOnCondition(
                query, params, null, null, EntityProcessManager.class);
        if(value != null && value.size() > 0){
            return value.get(0);
        }
        return null;
    }

    /**
     * 
     * @param strKey
     * @return 
     */
    public boolean insertProcessToData(String strKey) {
        StringBuilder sqlUpdate = new StringBuilder();
        List<Object> arrParams = new ArrayList<>();
        sqlUpdate.append(" insert into system_process_manager(code,dateexcute,idnumber,content) values(?,sysdate,?,?)");
        arrParams.add(strKey);
        arrParams.add(FunctionCommon.KEYIDPROCESS);
        arrParams.add("key cấu hình đánh dấu tiến trình chạy đồng bộ");
        CommonDataBaseDao cmd = new CommonDataBaseDao();
        return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
    }

    /**
     * thuc hien update lai thoi gian hoac id process thanh tien trinh dang chay
     * neu da vuot qua thoi gian 
     * @param strKey
     * @param keyIdProcess 
     * @return  
     */
    public boolean updateProcessToData(String strKey, String keyIdProcess) {            
        try {
            InetAddress IP= InetAddress.getLocalHost();
            StringBuilder sqlUpdate = new StringBuilder();
            List<Object> arrParams = new ArrayList<>();
            sqlUpdate.append(" update system_process_manager set idnumber = ?, dateexcute = sysdate,content =? where code = ? ");
            arrParams.add(keyIdProcess);
            arrParams.add("key cấu hình đánh dấu tiến trình chạy đồng bộ: " + IP.getHostAddress());
            arrParams.add(strKey);
            CommonDataBaseDao cmd = new CommonDataBaseDao();
            return cmd.insertOrUpdateDataBase(sqlUpdate, arrParams);
        } catch (UnknownHostException ex) {
            System.out.println(ex);
        }
        return false;
    }
}
