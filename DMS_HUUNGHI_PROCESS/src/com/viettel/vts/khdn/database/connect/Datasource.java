/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.database.connect;

/**
 *
 * @author datnv5
 */
import com.mchange.v2.c3p0.ComboPooledDataSource;
import com.viettel.vts.khdn.constants.FunctionCommon;
import com.viettel.vts.khdn.security.SecurityUtil;
import java.beans.PropertyVetoException;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.Properties;
import org.apache.log4j.Logger;

public class Datasource {

    /**
     * A singleton that represents a pooled datasource. It is composed of a C3PO
     * pooled datasource. Can be changed to any connect pool provider
     */
    private Properties props;
    private ComboPooledDataSource cpds;
    private static Datasource datasource;
    private static Logger log = Logger.getLogger(Datasource.class);
    private static Date dateCheckPool = new Date();
    
    private Datasource() throws IOException, SQLException {
        try {
            log.info("Reading DataVoffice2.properties from classpath");
            props = FunctionCommon.readProperties("com/viettel/vts/khdn/database/config/DataCommon.properties");
            
            SecurityUtil securityUtil = new SecurityUtil();
            String strfile =props.getProperty("urlfileconnect");
            ClassLoader loader = Thread.currentThread().getContextClassLoader();
            InputStream stream = loader.getResourceAsStream(strfile);
            String decryptString = securityUtil.decryptFile(stream);
            String[] properties = decryptString.split("\r\n");
            String server = "";
            String userName = "";
            String password = "";
            if (properties.length >= 3)
            {
                server = properties[0].trim().replace("hibernate.connection.url=", "").trim();
                userName = properties[1].trim().replace("hibernate.connection.username=", "").trim();
                password = properties[2].trim().replace("hibernate.connection.password=", "").trim();
            }
            server = "jdbc:oracle:thin:@10.60.157.150:1521:db18c";
            userName = "DMSZOTT";
            password = "DMSZOTT#123";
            cpds = new ComboPooledDataSource();
            cpds.setDriverClass(props.getProperty("driverClass"));
            cpds.setJdbcUrl(server);
            cpds.setUser(userName);
            cpds.setPassword(password);
            cpds.setInitialPoolSize(new Integer((String) props.getProperty("initialPoolSize")));
            cpds.setAcquireIncrement(new Integer((String) props.getProperty("acquireIncrement")));
            cpds.setMaxPoolSize(new Integer((String) props.getProperty("maxPoolSize")));
            cpds.setMinPoolSize(new Integer((String) props.getProperty("minPoolSize")));
            cpds.setMaxStatements(new Integer((String) props.getProperty("maxStatements")));
            cpds.setIdleConnectionTestPeriod(new Integer((String) props.getProperty("idleConnectionTestPeriod")));
            cpds.setCheckoutTimeout(new Integer((String) props.getProperty("checkoutTimeout")));
            //Xuly loi APPARENT DEADLOCK
            cpds.setStatementCacheNumDeferredCloseThreads(new Integer((String) props.getProperty("statementCacheNumDeferredCloseThreads")));
        } catch (PropertyVetoException ex) {
            log.error(ex);
        }
    }

    public static synchronized Datasource getInstance()
            throws IOException, SQLException {

        if (datasource != null) {
            return datasource;
        }
        datasource = new Datasource();
        return datasource;
    }
    
    public static synchronized Datasource getInstanceLossConnect()
            throws IOException, SQLException {
        datasource = new Datasource();
        return datasource;
    }
    
    public Connection getConnection() throws SQLException {
        Date datenow = new Date();
        long diff = 5;
        if (dateCheckPool != null) {
            diff = (datenow.getTime() - dateCheckPool.getTime())/1000;
        }
        if (diff > 240) {
            try {
                log.info("getNumBusyConnections = " + cpds.getNumBusyConnections()
                        + ",  NumConnections=" + cpds.getNumConnections());
            } catch (SQLException e) {
                log.info("getConnection:", e);
            }
            dateCheckPool = new Date();
        }

        return this.cpds.getConnection();
    }
}
