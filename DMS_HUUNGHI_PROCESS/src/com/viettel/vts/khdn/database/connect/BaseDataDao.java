/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.viettel.vts.khdn.database.connect;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import org.apache.log4j.Logger;

/**
 * Lop quan ly ket noi toi dataVoffice 1.0
 * @author datnv5
 */
public class BaseDataDao {

    /** Ket noi den DB */
    protected Connection conn = null;
    /** loger for this class */
    private static Logger log = Logger.getLogger(BaseDataDao.class);

    /**
     * Phuong thuc khoi tao
     */
    public BaseDataDao() {
    }

    public Connection openConnection() {
        try {
            Datasource ds = Datasource.getInstance();
            conn = ds.getConnection();
        } catch (IOException ex) {
            log.error("Loi! Khong doc duoc file properties cau hinh database Voffice: ", ex);
        } catch (SQLException ex) {
            try {
                log.error("Loi! Khong mo duoc ket noi Data Base Voffice 2: ", ex);
                Datasource ds = Datasource.getInstanceLossConnect();
                conn = ds.getConnection();
            } catch (IOException | SQLException ex1) {
                log.error("Loi! Khong mo duoc ket noi Data Base Voffice 2 lan 2: ", ex1);
            }
        }
        return conn;
    }

    /**
     * Close a connection avoid closing if null and
     */
    public void closeConnection() {
        if ((conn != null)) {
            try {
                conn.close();
                conn = null;
                this.conn = null;
            } catch (SQLException e) {
                log.error("Loi! Khong dong duoc ket noi dataBase 2: ");
                log.error(e);
            }
        }
    }
}
