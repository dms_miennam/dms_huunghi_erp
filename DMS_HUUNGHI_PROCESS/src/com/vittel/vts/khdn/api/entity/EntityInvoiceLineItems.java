package com.vittel.vts.khdn.api.entity;

import java.math.BigInteger;

public class EntityInvoiceLineItems {
	private String invoiceHeaderId;
	private String lineNumber;
	private String deliveryNoteId;
	private String deliveryNoteLineNumber;
	private String productId;
	private String unitId;
	private Integer quantity;
	private boolean isPromotion;
	private String priceTypeId;
	private Integer price;
	private BigInteger amount;
	private BigInteger taxAmount;
	public String getInvoiceHeaderId() {
		return invoiceHeaderId;
	}
	public void setInvoiceHeaderId(String invoiceHeaderId) {
		this.invoiceHeaderId = invoiceHeaderId;
	}
	public String getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getDeliveryNoteId() {
		return deliveryNoteId;
	}
	public void setDeliveryNoteId(String deliveryNoteId) {
		this.deliveryNoteId = deliveryNoteId;
	}
	public String getDeliveryNoteLineNumber() {
		return deliveryNoteLineNumber;
	}
	public void setDeliveryNoteLineNumber(String deliveryNoteLineNumber) {
		this.deliveryNoteLineNumber = deliveryNoteLineNumber;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public boolean isPromotion() {
		return isPromotion;
	}
	public void setPromotion(boolean isPromotion) {
		this.isPromotion = isPromotion;
	}
	public String getPriceTypeId() {
		return priceTypeId;
	}
	public void setPriceTypeId(String priceTypeId) {
		this.priceTypeId = priceTypeId;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public BigInteger getAmount() {
		return amount;
	}
	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}
	public BigInteger getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(BigInteger taxAmount) {
		this.taxAmount = taxAmount;
	}
	
	
}
