package com.vittel.vts.khdn.api.entity;

public class EntityDeliveryNoteLineItems {
	private String deliveryNoteHeaderId;
	private String lineNumber;
	private String salesOrderId;
	private String salesOrderLineNumber;
	private String deliveryOrderId;
	private String deliveryOrderLineNumber;
	private String productId;
	private String unitId;
	private Integer quantity;
	public String getDeliveryNoteHeaderId() {
		return deliveryNoteHeaderId;
	}
	public void setDeliveryNoteHeaderId(String deliveryNoteHeaderId) {
		this.deliveryNoteHeaderId = deliveryNoteHeaderId;
	}
	public String getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getSalesOrderId() {
		return salesOrderId;
	}
	public void setSalesOrderId(String salesOrderId) {
		this.salesOrderId = salesOrderId;
	}
	public String getSalesOrderLineNumber() {
		return salesOrderLineNumber;
	}
	public void setSalesOrderLineNumber(String salesOrderLineNumber) {
		this.salesOrderLineNumber = salesOrderLineNumber;
	}
	public String getDeliveryOrderId() {
		return deliveryOrderId;
	}
	public void setDeliveryOrderId(String deliveryOrderId) {
		this.deliveryOrderId = deliveryOrderId;
	}
	public String getDeliveryOrderLineNumber() {
		return deliveryOrderLineNumber;
	}
	public void setDeliveryOrderLineNumber(String deliveryOrderLineNumber) {
		this.deliveryOrderLineNumber = deliveryOrderLineNumber;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
}
