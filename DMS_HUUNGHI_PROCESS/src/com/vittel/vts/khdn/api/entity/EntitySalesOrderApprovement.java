package com.vittel.vts.khdn.api.entity;

import java.sql.Date;
import java.util.List;

public class EntitySalesOrderApprovement {
	private String id;
	private String name;
	private Date createAt;
	private Date lastModifiedAt;
	private String salesOrderId;
	private String salesOrderCode;
	private String note;
	private List<EntitySalesOrderApprovementLineItem> list;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public Date getLastModifiedAt() {
		return lastModifiedAt;
	}
	public void setLastModifiedAt(Date lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}
	public String getSalesOrderId() {
		return salesOrderId;
	}
	public void setSalesOrderId(String salesOrderId) {
		this.salesOrderId = salesOrderId;
	}
	public String getSalesOrderCode() {
		return salesOrderCode;
	}
	public void setSalesOrderCode(String salesOrderCode) {
		this.salesOrderCode = salesOrderCode;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public List<EntitySalesOrderApprovementLineItem> getList() {
		return list;
	}
	public void setList(List<EntitySalesOrderApprovementLineItem> list) {
		this.list = list;
	}
	
	
}
