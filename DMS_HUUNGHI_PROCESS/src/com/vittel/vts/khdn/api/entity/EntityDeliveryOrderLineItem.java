package com.vittel.vts.khdn.api.entity;

import java.math.BigInteger;
import java.util.Date;

public class EntityDeliveryOrderLineItem {
	private String deliveryOrderHeaderId;
	private String productId;
	private String unitId;
	private Integer proposeQuantity;
	private boolean isPromotion;
	private String priceTypeId;
	private BigInteger price;
	private Integer total;
	private String taxId;
	private Integer taxRate;
	private Integer taxAmount;
	private Date deliveryDate;
	private String note;
	private String deliveryOrderHeader;
	
	public String getDeliveryOrderHeaderId() {
		return deliveryOrderHeaderId;
	}
	public void setDeliveryOrderHeaderId(String deliveryOrderHeaderId) {
		this.deliveryOrderHeaderId = deliveryOrderHeaderId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Integer getProposeQuantity() {
		return proposeQuantity;
	}
	public void setProposeQuantity(Integer proposeQuantity) {
		this.proposeQuantity = proposeQuantity;
	}
	public boolean isPromotion() {
		return isPromotion;
	}
	public void setPromotion(boolean isPromotion) {
		this.isPromotion = isPromotion;
	}
	public String getPriceTypeId() {
		return priceTypeId;
	}
	public void setPriceTypeId(String priceTypeId) {
		this.priceTypeId = priceTypeId;
	}
	public BigInteger getPrice() {
		return price;
	}
	public void setPrice(BigInteger price) {
		this.price = price;
	}
	public Integer getTotal() {
		return total;
	}
	public void setTotal(Integer total) {
		this.total = total;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public Integer getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Integer taxRate) {
		this.taxRate = taxRate;
	}
	public Integer getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(Integer taxAmount) {
		this.taxAmount = taxAmount;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getDeliveryOrderHeader() {
		return deliveryOrderHeader;
	}
	public void setDeliveryOrderHeader(String deliveryOrderHeader) {
		this.deliveryOrderHeader = deliveryOrderHeader;
	}
	
	
	
}
