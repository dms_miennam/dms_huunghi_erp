package com.vittel.vts.khdn.api.entity;

import java.util.Date;
import java.util.List;

public class EntityInvoices {

	private String id;
	private String code;
	private Date date;
	private String channelId;
	private String customerId;
	private Date createdAt;
	private Date lastModifiedAt;
	private String deliveryOrderId;
	private List<EntityInvoiceLineItems> invoiceLineItems;
	
	public String getDeliveryOrderId() {
		return deliveryOrderId;
	}
	public void setDeliveryOrderId(String deliveryOrderId) {
		this.deliveryOrderId = deliveryOrderId;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getLastModifiedAt() {
		return lastModifiedAt;
	}
	public void setLastModifiedAt(Date lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}
	public List<EntityInvoiceLineItems> getInvoiceLineItems() {
		return invoiceLineItems;
	}
	public void setInvoiceLineItems(List<EntityInvoiceLineItems> invoiceLineItems) {
		this.invoiceLineItems = invoiceLineItems;
	}
	
}