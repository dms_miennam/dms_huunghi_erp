package com.vittel.vts.khdn.api.entity;

public class EntityDeliveryActualLineItem {
	private String deliveryActualHeaderId;
	private String productId;
	private String unitId;
	private Integer proposedQuantity;
	private Integer actualQuantity;
	private String deliveryActualHeader;
	
	public String getDeliveryActualHeaderId() {
		return deliveryActualHeaderId;
	}
	public void setDeliveryActualHeaderId(String deliveryActualHeaderId) {
		this.deliveryActualHeaderId = deliveryActualHeaderId;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Integer getProposedQuantity() {
		return proposedQuantity;
	}
	public void setProposedQuantity(Integer proposedQuantity) {
		this.proposedQuantity = proposedQuantity;
	}
	public Integer getActualQuantity() {
		return actualQuantity;
	}
	public void setActualQuantity(Integer actualQuantity) {
		this.actualQuantity = actualQuantity;
	}
	public String getDeliveryActualHeader() {
		return deliveryActualHeader;
	}
	public void setDeliveryActualHeader(String deliveryActualHeader) {
		this.deliveryActualHeader = deliveryActualHeader;
	}
	
	
}
