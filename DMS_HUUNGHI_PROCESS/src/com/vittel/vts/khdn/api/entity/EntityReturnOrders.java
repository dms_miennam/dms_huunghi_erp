package com.vittel.vts.khdn.api.entity;

import java.util.Date;
import java.util.List;

public class EntityReturnOrders {
	private String id;
	private String code;
	private Date date;
	private String dmsRefCode;
	private String returnNotificationId;
	private String channelId;
	private String customerId;
	private String shippingAddressId;
	private String contact;
	private String status;
	private String note;
	private Date createdAt;
	private Date lastModifiedAt;
	private List<EntityReturnOrderLineItems> returnOrderLineItems ;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getDmsRefCode() {
		return dmsRefCode;
	}
	public void setDmsRefCode(String dmsRefCode) {
		this.dmsRefCode = dmsRefCode;
	}
	public String getReturnNotificationId() {
		return returnNotificationId;
	}
	public void setReturnNotificationId(String returnNotificationId) {
		this.returnNotificationId = returnNotificationId;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getShippingAddressId() {
		return shippingAddressId;
	}
	public void setShippingAddressId(String shippingAddressId) {
		this.shippingAddressId = shippingAddressId;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getCreatedAt() {
		return createdAt;
	}
	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}
	public Date getLastModifiedAt() {
		return lastModifiedAt;
	}
	public void setLastModifiedAt(Date lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}
	public List<EntityReturnOrderLineItems> getReturnOrderLineItems() {
		return returnOrderLineItems;
	}
	public void setReturnOrderLineItems(List<EntityReturnOrderLineItems> returnOrderLineItems) {
		this.returnOrderLineItems = returnOrderLineItems;
	}

}
