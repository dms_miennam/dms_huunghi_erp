package com.vittel.vts.khdn.api.entity;

import java.math.BigInteger;
import java.util.Date;

public class EntityReturnOrderLineItems {
	private String returnOrderHeaderId;
	private String lineNumber;
	private String productId;
	private String unitId;
	private Integer quantity;
	private Integer price;
	private BigInteger amount;
	private boolean isPromotion;
	private String priceTypeId;
	private String taxId;
	private Integer taxRate;
	private BigInteger taxAmount;
	private Date shipDate;
	public String getReturnOrderHeaderId() {
		return returnOrderHeaderId;
	}
	public void setReturnOrderHeaderId(String returnOrderHeaderId) {
		this.returnOrderHeaderId = returnOrderHeaderId;
	}
	public String getLineNumber() {
		return lineNumber;
	}
	public void setLineNumber(String lineNumber) {
		this.lineNumber = lineNumber;
	}
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public String getUnitId() {
		return unitId;
	}
	public void setUnitId(String unitId) {
		this.unitId = unitId;
	}
	public Integer getQuantity() {
		return quantity;
	}
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	public Integer getPrice() {
		return price;
	}
	public void setPrice(Integer price) {
		this.price = price;
	}
	public BigInteger getAmount() {
		return amount;
	}
	public void setAmount(BigInteger amount) {
		this.amount = amount;
	}
	public boolean isPromotion() {
		return isPromotion;
	}
	public void setPromotion(boolean isPromotion) {
		this.isPromotion = isPromotion;
	}
	public String getPriceTypeId() {
		return priceTypeId;
	}
	public void setPriceTypeId(String priceTypeId) {
		this.priceTypeId = priceTypeId;
	}
	public String getTaxId() {
		return taxId;
	}
	public void setTaxId(String taxId) {
		this.taxId = taxId;
	}
	public Integer getTaxRate() {
		return taxRate;
	}
	public void setTaxRate(Integer taxRate) {
		this.taxRate = taxRate;
	}
	public BigInteger getTaxAmount() {
		return taxAmount;
	}
	public void setTaxAmount(BigInteger taxAmount) {
		this.taxAmount = taxAmount;
	}
	public Date getShipDate() {
		return shipDate;
	}
	public void setShipDate(Date shipDate) {
		this.shipDate = shipDate;
	}
	
}
