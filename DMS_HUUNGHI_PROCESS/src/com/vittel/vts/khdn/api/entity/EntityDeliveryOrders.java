package com.vittel.vts.khdn.api.entity;

import java.util.Date;
import java.util.List;

public class EntityDeliveryOrders {
	private String id;
	private String code;
	private Date date;
	private String channelId;
	private String customerId;
	private String shippingAddressId;
	private String productGroupId;
	private String contact;
	private String status;
	private String note;
	private Date createAt;
	private Date lastModifiedAt;
	private boolean isUnscheduled;
	private List<EntityDeliveryOrderLineItems> deliveryOrderLineItems;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public String getChannelId() {
		return channelId;
	}
	public void setChannelId(String channelId) {
		this.channelId = channelId;
	}
	public String getCustomerId() {
		return customerId;
	}
	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}
	public String getShippingAddressId() {
		return shippingAddressId;
	}
	public void setShippingAddressId(String shippingAddressId) {
		this.shippingAddressId = shippingAddressId;
	}
	public String getProductGroupId() {
		return productGroupId;
	}
	public void setProductGroupId(String productGroupId) {
		this.productGroupId = productGroupId;
	}
	public String getContact() {
		return contact;
	}
	public void setContact(String contact) {
		this.contact = contact;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Date getCreateAt() {
		return createAt;
	}
	public void setCreateAt(Date createAt) {
		this.createAt = createAt;
	}
	public Date getLastModifiedAt() {
		return lastModifiedAt;
	}
	public void setLastModifiedAt(Date lastModifiedAt) {
		this.lastModifiedAt = lastModifiedAt;
	}
	public boolean isUnscheduled() {
		return isUnscheduled;
	}
	public void setUnscheduled(boolean isUnscheduled) {
		this.isUnscheduled = isUnscheduled;
	}
	public List<EntityDeliveryOrderLineItems> getDeliveryOrderLineItems() {
		return deliveryOrderLineItems;
	}
	public void setDeliveryOrderLineItems(List<EntityDeliveryOrderLineItems> deliveryOrderLineItems) {
		this.deliveryOrderLineItems = deliveryOrderLineItems;
	}
	
}