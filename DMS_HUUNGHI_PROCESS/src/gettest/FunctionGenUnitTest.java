/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gettest;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.lang.annotation.Annotation;
import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author KITTY
 */
public class FunctionGenUnitTest {

    public static void main(String[] args) {
        genClass("com.viettel.vts.khdn.security.tesst");

    }

    private static void genClass(String sourcePath) {

        if (sourcePath == null) {
            return;
        }
        String testPath = "test";
        final File fileclass = new File(sourcePath);

        try {
            Class clazz = Class.forName(sourcePath);
            String pathClass = clazz.getCanonicalName();
            String strClassTest = testPath + "." + pathClass + "Test";
            strClassTest = strClassTest.replace(".", "/");
            File file = new File(strClassTest + ".java");
            if (!file.exists()) {
//                System.out.println(file.getName() + " exist");
                file.getParentFile().mkdirs();
            }

            PrintWriter printWriteAction = new PrintWriter(file);

            Throwable var11 = null;
            try {
                StringBuilder strContentCodeAction = genarateClassTest(pathClass, clazz);
                printWriteAction.print(strContentCodeAction);
            } catch (IOException | ClassNotFoundException | NoSuchMethodException var21) {
                var11 = var21;
                throw var21;
            } finally {
                if (printWriteAction != null) {
                    if (var11 != null) {
                        try {
                            printWriteAction.close();
                        } catch (Throwable var20) {
                            var11.addSuppressed(var20);
                        }
                    } else {
                        printWriteAction.close();
                    }
                }
            }
        } catch (IOException | ClassNotFoundException | NoSuchMethodException e) {
            System.out.println(e.getMessage());
        }

    }

    private static StringBuilder genarateClassTest(String pathClass, Class clazz) throws IOException, NoSuchMethodException, ClassNotFoundException {
        String className = pathClass.substring(pathClass.lastIndexOf(".") + 1);
        System.out.println(className);
        String pathClassConvert = "src\\" + pathClass.replace(".", "\\") + ".java";
        FileInputStream fr = new FileInputStream(pathClassConvert);
        BufferedReader br = new BufferedReader(new InputStreamReader(fr));
        StringBuilder strContentCodeAction = new StringBuilder();
        List<String> importCodes = new ArrayList<>();
        String line = br.readLine();

        //gen import
        while (line != null) {
            if (line.startsWith("package") || line.startsWith("import")) {
                strContentCodeAction.append(line).append("\r");
                importCodes.add(line);
            }

            if (line.contains(className)) {
                break;
            }
            line = br.readLine();
        }
        strContentCodeAction.append("import ").append(pathClass).append(";\r\n");
        strContentCodeAction.append("import org.hamcrest.MatcherAssert;\n");
//        strContentCodeAction.append("import org.springframework.http.HttpStatus;\n");
//        strContentCodeAction.append("import org.springframework.http.MediaType;\n");
//        strContentCodeAction.append("import org.hamcrest.MatcherAssert;\n");
        strContentCodeAction.append("import static org.hamcrest.MatcherAssert.assertThat;\n");
        strContentCodeAction.append("import org.hamcrest.Matchers;\n");
        strContentCodeAction.append("import org.junit.jupiter.api.BeforeEach;\n");
        strContentCodeAction.append("import org.junit.jupiter.api.Test;\n");
        strContentCodeAction.append("import org.mockito.junit.jupiter.MockitoExtension;\n");
        strContentCodeAction.append("import static org.mockito.Mockito.when;\n");
        strContentCodeAction.append("import org.junit.jupiter.api.Assertions;\n");
        strContentCodeAction.append("import com.fasterxml.jackson.databind.ObjectMapper;\n");
        strContentCodeAction.append("import org.mockito.InjectMocks;\n");
        strContentCodeAction.append("import org.mockito.Mock;\n");
        strContentCodeAction.append("import org.mockito.Mockito;\n");
        strContentCodeAction.append("import org.mockito.MockitoAnnotations;\n\n");

        strContentCodeAction.append("class ").append(className).append("Test {\n\n");

        //read remain file to get autowired variable and method
        Map<String, String> mockVariables = new HashMap<>();
        StringBuilder code2 = new StringBuilder();
        while (line != null) {
            code2.append(line).append("\n");
            line = br.readLine();
        }

        //format code
        String[] formatCodes = formatCode(code2.toString()).split("\n");
        for (String lineCode : formatCodes) {
            if (lineCode.contains("@Autowired")) {
                strContentCodeAction.append("\t@Mock" + "\n");
                strContentCodeAction.append("\t").append(lineCode.replace("@Autowired", "").trim()).append("\n");
                String[] variables = lineCode.replace(";", "").trim().split(" ");
                mockVariables.put(variables[variables.length - 1], variables[variables.length - 2]);
            }
        }

        //write inject mock
        strContentCodeAction.append("\t@InjectMocks \r");
        String classVariable = "service";
        strContentCodeAction.append("\t").append(className).append(" ").append(classVariable).append(";\r\r");
        strContentCodeAction.append("\t@BeforeEach\n");
        strContentCodeAction.append("\tvoid setUp() {\n");
        strContentCodeAction.append("\t\t").append(classVariable).append(" = new ").append(className).append("();\n");
        strContentCodeAction.append("\t\tMockitoAnnotations.initMocks(this);\n");
        strContentCodeAction.append("\t}\n\n");

        // gen function
        // reset file again to find method
        String temp = "";
        ArrayList<String> codes = new ArrayList<>();
        for (int i = 0; i < formatCodes.length; i++) {
            line = formatCodes[i];
            // split file to multi function
            if (matchMethodRegex(line)) {
                String code = formatCode(temp);
                if (code != null && code.trim().length() > 0) {
                    codes.add(code);
                }
                temp = "";
            }
            temp = temp + line;

            if (i == formatCodes.length - 1) {
                String code = formatCode(line);
                if (code != null && code.trim().length() > 0) {
                    codes.add(code);
                }
                break;
            }
        }

        // gen function
        Method[] methods = TestUtil.getAccessibleMethods(clazz);
        for (int i = 0; i < codes.size(); i++) {
            for (int j = 0; j < methods.length; j++) {
                Method method = methods[j];
                String methodLine = codes.get(i).split("\n")[0];
                // equal method name and number of argument
                try {
                    if (isLineContainMethod(methodLine, method)) {
                        System.out.println(methodLine);
                        //testcase 1: status ok
                        strContentCodeAction.append(genFunction(codes.get(i), method, mockVariables, importCodes, j, false));

                        //testcase 2: exception
                        strContentCodeAction.append(genFunction(codes.get(i), method, mockVariables, importCodes, j, true));
                    }
                } catch (Exception e) {
                    System.out.println("Không gen test được cho " + className + "." + method.getName());
                    e.printStackTrace();
                }
            }
        }

        strContentCodeAction.append("}");

        return strContentCodeAction;
    }

    private static String formatCode(String code) {
        if (code.startsWith("package")) {
            return "";
        }
        // match comment
        code = code.replaceAll("//.*|(\"(?:\\\\[^\"]|\\\\\"|.)*?\")|(?s)/\\*.*?\\*/", "$1 ");

        code = code.replaceAll("\n+", " ");
        code = code.replaceAll("\t+", " ");
        code = code.replaceAll("\\s+", " ");
        //replace Override
        code = code.replaceAll("@Override", "");

        code = code.trim() + "\n";
        code = code.replaceAll(";", ";\n");

        code = code.replaceAll("\\{", "\\{\n");
        code = code.replaceAll("\\}", "\\}\n");
        return code;
    }

    private static boolean matchMethodRegex(String line) {
        String methodRegex = "(public|private|static|protected|abstract|native|synchronized) +([a-zA-Z0-9<>._?, ]+) +([a-zA-Z0-9_]+) *\\([a-zA-Z0-9<>\\[\\]._?, \n]*\\) *([a-zA-Z0-9_ ,\n]*) *\\{";
        Pattern pattern = Pattern.compile(methodRegex);
        Matcher matcher = pattern.matcher(line);
        return matcher.find();
    }

    private static boolean isLineContainMethod(String methodLine, Method method) {
        if ((methodLine.contains(" " + method.getName() + "(") || methodLine.contains(" " + method.getName() + " ("))
                && method.getParameters().length == methodLine.split(",").length) {
            Parameter[] parameters = method.getParameters();
            int beginParam = methodLine.indexOf("(");
            int endParam = methodLine.lastIndexOf(")");
            if (beginParam == -1 || endParam == -1) {
                return false;
            }
            String[] params = methodLine.substring(beginParam, endParam).split(",");
            for (int i = 0; i < params.length; i++) {
                if (!params[i].contains(parameters[i].getType().getSimpleName())) {
                    return false;
                }
            }
            return true;

        }
        return false;
    }

    private static StringBuilder genFunction(String code, Method method, Map<String, String> mockVariables,
            List<String> importCodes, int methodSTT, boolean genExceptions) throws IOException, ClassNotFoundException {
        StringBuilder strContentCodeAction = new StringBuilder();
        strContentCodeAction.append("\t@Test\n");
        String methodName = method.getName() + methodSTT;
        if (genExceptions) {
            strContentCodeAction.append("\tvoid " + methodName + "ThrowException" + code.split("\n")[1].split(",").length + "() throws Exception {\n");
        } else {
            strContentCodeAction.append("\tvoid " + methodName + "() throws Exception {\n");
        }
        // gen params
        strContentCodeAction.append("\t\t//TODO: sua dau vao phu hop voi nghiep vu\n");
        Parameter[] params = method.getParameters();
        initParams(strContentCodeAction, params);

        // gen mocks
        strContentCodeAction.append(genMocks(code, mockVariables, importCodes, genExceptions));

        // gen service invoke method
        String invokeMethod = "service." + method.getName() + "(";
        for (int i = 0; i < method.getParameterCount(); i++) {
            invokeMethod = invokeMethod + method.getParameters()[i].getName();
            if (i != method.getParameterCount() - 1) {
                invokeMethod = invokeMethod + ",";
            }
        }
        invokeMethod = invokeMethod + ")";
        if (!method.getReturnType().getSimpleName().equals("void") && !genExceptions) {
            strContentCodeAction.append("\n");
            strContentCodeAction.append("\t\t// invoke method\n");
            invokeMethod = method.getReturnType().getSimpleName() + " actualResult = " + invokeMethod;
            strContentCodeAction.append("\t\t" + invokeMethod + ";\n");
        } else if (method.getReturnType().getSimpleName().equals("void") && !genExceptions) {
            strContentCodeAction.append("\t\t" + invokeMethod + ";\n");
        }

        strContentCodeAction.append("\n");
        // gen assert
        if (genExceptions) {
            //test case 2: catch exception
            strContentCodeAction.append("\t\t//TODO: thay doi assert result phu hop voi nghiep vu\n");
            strContentCodeAction.append("\t\tAssertions.assertThrows(Exception.class, () -> {\n");
            strContentCodeAction.append("\t\t\t" + invokeMethod + ";\n");
            strContentCodeAction.append("\t\t});\n");
        } else {
            //test case 1: status is ok
            if (!method.getReturnType().getSimpleName().equals("void")) {
                strContentCodeAction.append("\t\t//TODO: thay doi assert result phu hop voi nghiep vu\n");
                strContentCodeAction.append("\t\tassertThat(actualResult, Matchers.notNullValue());\n");
            }
        }
        strContentCodeAction.append("\t}\n\n");
        return strContentCodeAction;
    }

    private static void initParams(StringBuilder strContentCodeAction, Parameter[] params) {
        for (Parameter param : params) {
            strContentCodeAction.append("\t\t" + param.getType().getSimpleName());
            strContentCodeAction.append(" " + param.getName() + " = ");
            boolean needGenerateValidParam = false;
            for (Annotation annotation : param.getAnnotations()) {
                if (annotation.annotationType().getSimpleName().contains("Valid")) {
                    needGenerateValidParam = true;
                }
            }
            strContentCodeAction.append(initDefaultValue(param.getType(), param.getName(), needGenerateValidParam));
        }
    }

    private static String initDefaultValue(Class clazz, String variableName, boolean needGenerateValidParam) {
        StringBuilder strContentCodeAction = new StringBuilder();
        switch (clazz.getSimpleName()) {
            case "Integer":
            case "int":
                strContentCodeAction.append("0;\n");
                break;
            case "String":
                strContentCodeAction.append("new String(\"test\");\n");
                break;
            case "Long":
            case "long":
                strContentCodeAction.append("0L;\n");
                break;
            case "Double":
            case "double":
                strContentCodeAction.append("new Double(0);\n");
                break;
            case "Byte":
                strContentCodeAction.append("new Byte(\"test\");\n");
                break;
            case "Boolean":
            case "boolean":
                strContentCodeAction.append("true;\n");
                break;
            case "Character":
                strContentCodeAction.append("new Character('0'); ;\n");
                break;
            case "Short":
                strContentCodeAction.append("new Short(0);\n");
                break;
            case "Float":
                strContentCodeAction.append("new Float(0);\n");
                break;
            case "Authentication":
                strContentCodeAction.append("null;\n");
                break;
            case "Optional":
                strContentCodeAction.append("Optional.empty();\n");
                break;
            case "List":
                strContentCodeAction.append("new ArrayList<>();\n");
                break;
            default:
                //handle array later
                if (clazz.isArray()) {
                    strContentCodeAction.append("null;\n");
                } else
                    try {
                    Constructor constructor = clazz.getConstructor();
                    strContentCodeAction.append("new " + clazz.getSimpleName() + "();\n");
                    if (needGenerateValidParam) {
                        strContentCodeAction.append("\t\t// TODO sua cac tham so sau de khop voi dieu kien trong DTO\n");
                        strContentCodeAction.append(initParamsValidAnnotation(clazz, variableName));
                    }

                } catch (NoSuchMethodException e) {
                    strContentCodeAction.append("null;\n");
                    System.out.println(clazz.getSimpleName() + " không có no arg constructor");
                }
        }
        return strContentCodeAction.toString();
    }

    private static StringBuilder initParamsValidAnnotation(Class clazz, String variableName) {
        StringBuilder strContentCodeAction = new StringBuilder();
        for (Field field : clazz.getDeclaredFields()) {
            for (Annotation annotation : field.getAnnotations()) {
                if (annotation.annotationType().getSimpleName().contains("NotNull")) {
                    strContentCodeAction.append("\t\t" + variableName + ".set"
                            + field.getName().substring(0, 1).toUpperCase() + field.getName().substring(1));
                    strContentCodeAction.append("(" + initDefaultValue(field.getType(), field.getName(), false).replace(";", ");"));
                }
            }
        }
        return strContentCodeAction;
    }

    private static StringBuilder genMocks(String code, Map<String, String> mockVariables, List<String> importCodes, boolean genException) throws ClassNotFoundException {
        StringBuilder strContentCodeAction = new StringBuilder();
        String[] lines = code.split("\n");
        int count = 0;
        for (String line : lines) {
            for (String variable : mockVariables.keySet()) {
                line = line.replaceAll("\\s+\\.", ".");
                if (line.contains(" " + variable + ".") || line.contains("(" + variable + ".") || line.contains("this." + variable + ".")) {
                    if (count == 0) {
                        strContentCodeAction.append("\n");
                        strContentCodeAction.append("\t\t//TODO: sua dau ra cua mock method de tao thanh cac testcase khac\n");
                    }
                    count++;
                    // get method name
                    int begin = line.indexOf(variable);
                    String methodMock = getMethodMockInLine(line, variable);
                    int beginArg = methodMock.indexOf("(");
                    int numArg = methodMock.substring(beginArg).split(",").length;
                    String methodName = methodMock.substring(methodMock.indexOf(".") + 1, methodMock.indexOf("("));
                    System.out.println(methodMock.indexOf(".") + 1 + "-" + methodMock.indexOf("("));
                    methodMock = methodMock.substring(0, beginArg + 1);
                    for (int i = 0; i < numArg; i++) {
                        methodMock = methodMock + "Mockito.any(),";
                    }
                    methodMock = methodMock.substring(methodMock.indexOf("." + methodName) + 1, methodMock.length() - 1) + ")";

                    //get class
                    for (String importCode : importCodes) {
                        if (importCode.contains(mockVariables.get(variable) + ";")) {
                            Class clazz = Class.forName(importCode
                                    .replace("import", "")
                                    .replace(";", "")
                                    .trim());
                            for (Method method : clazz.getMethods()) {
                                if (method.getName().equals(methodName) && method.getParameters().length == numArg) {
                                    // gen default exception
                                    if (genException) {
                                        if (method.getExceptionTypes().length > 0) {
                                            // gen void method
                                            if (method.getReturnType().getSimpleName().equals("void")) {
                                                strContentCodeAction.append("\t\tMockito.doThrow(new Exception())"
                                                        + ".when(" + variable + ")." + methodMock + ";\n");
                                            } else {
                                                //gen normal method
                                                strContentCodeAction.append("\t\twhen(" + variable + "." + methodMock + ").thenThrow(new Exception());\n");
                                            }
                                        }
                                    } else if (method.getReturnType().getSimpleName().equals("void")) {
                                        strContentCodeAction.append("\t\tMockito.doNothing().when(" + variable + ")." + methodMock + ";\n");
                                    } else if (method.getReturnType().getSimpleName().equals("Object")) {
                                        // gen default result
                                        strContentCodeAction.append("\t\tObject resultData" + count + " = new Object();\n");
                                        strContentCodeAction.append("\t\twhen(" + variable + "." + methodMock + ").thenReturn(resultData" + count + ");\n");
                                    } else {
                                        // gen default result
                                        if (method.getReturnType().getSimpleName().equals("Optional")) {
                                            strContentCodeAction.append("\t\t" + method.getReturnType().getSimpleName() + " resultData" + count + " = Optional.empty();\n");
                                        } else {
                                            strContentCodeAction.append("\t\t" + method.getReturnType().getSimpleName() + " resultData" + count + " = ");
                                            strContentCodeAction.append(initDefaultValue(method.getReturnType(), "resultData" + count, false));

                                        }
                                        strContentCodeAction.append("\t\twhen(" + variable + "." + methodMock + ").thenReturn(resultData" + count + ");\n");

                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
        return strContentCodeAction;
    }

    private static String getMethodMockInLine(String line, String variable) {
        line = line.replaceAll("\\s+", "");
        int begin = line.indexOf(variable);
        String methodMock = line.substring(begin).replace(";", "").replaceAll("\\s+", " ");
        int count = 0;
        int start = methodMock.indexOf("(");
        int endMethod = 0;
        for (int i = start; i < methodMock.length(); i++) {
            if (methodMock.charAt(i) == '(') {
                count++;
            } else if (methodMock.charAt(i) == ')') {
                count--;
            }
            if (count == 0) {
                endMethod = i;
                break;
            }
        }
        methodMock = methodMock.substring(0, endMethod + 1);
        return methodMock;
    }
}
